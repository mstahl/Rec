/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FutureCounterLevel.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FutureCounterLevel
//
// 2016-08-13 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( FutureCounterLevel )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FutureCounterLevel::FutureCounterLevel( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareInterface<IFutureCounterLevel>( this );

  // sync m_isQuiet and m_isVerbose with m_clevel
  m_clevel.declareUpdateHandler( [=]( const Property& ) {
    this->m_isQuiet   = ( this->m_clevel > 0 );
    this->m_isVerbose = ( this->m_clevel > 1 );
  } );
  m_clevel.useUpdateHandler();
}

//=============================================================================
