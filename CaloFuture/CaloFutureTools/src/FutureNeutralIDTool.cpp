/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
// from Event
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/RecHeader.h"

// local
#include "FutureNeutralIDTool.h"

using namespace LHCb;
using namespace Gaudi::Units;

//-----------------------------------------------------------------------------
// Implementation file for class : FutureNeutralIDTool
//
// 2013-07-25 : Mostafa HOBALLAH
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( FutureNeutralIDTool )

//=============================================================================
// Initialization
//=============================================================================
StatusCode FutureNeutralIDTool::initialize() {

  StatusCode sc = extends::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;       // error printed already by GaudiAlgorithm

  // TMVA discriminant
  m_reader1 = std::make_unique<ReadMLPH>(
      std::vector<std::string>{"TMath::TanH(CaloHypo_ClusterMatch/2000)", "CaloHypo_ToPrsE", "CaloHypo_E19",
                               "CaloHypo_Hcal2Ecal", "CaloHypo_PrsE19", "CaloHypo_PrsE49", "CaloHypo_Spread",
                               "CaloHypo_PrsE4Max", "CaloHypo_HypoPrsM", "CaloHypo_HypoSpdM"} );
  m_reader0 = std::make_unique<ReadMLPE>( std::vector<std::string>{
      "CaloHypo_ClusterMatch", "CaloHypo_ToPrsE", "CaloHypo_E19", "CaloHypo_Hcal2Ecal", "CaloHypo_PrsE19",
      "CaloHypo_PrsE49", "CaloHypo_Spread", "CaloHypo_PrsE4Max", "CaloHypo_HypoPrsM", "CaloHypo_HypoSpdM"} );

  return StatusCode{m_reader1->IsStatusClean() && m_reader0->IsStatusClean()};
}

//=============================================================================
// Main execution
//=============================================================================

std::optional<double> FutureNeutralIDTool::isNotE( const LHCb::CaloHypo&                                  hypo,
                                                   const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const {
  double pt = LHCb::CaloMomentum( &hypo ).pt();
  if ( pt <= m_minPt ) return std::nullopt;
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo, false );
  if ( !cluster ) return std::nullopt;
  return isNotE( v );
}

std::optional<double> FutureNeutralIDTool::isNotH( const LHCb::CaloHypo&                                  hypo,
                                                   const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const {
  double pt = LHCb::CaloMomentum( &hypo ).pt();
  if ( pt <= m_minPt ) return std::nullopt;
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo, false );
  if ( !cluster ) return std::nullopt;
  return isNotH( v );
}

double FutureNeutralIDTool::isNotE( const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const {
  return m_reader0->GetMvaValue(
      {tanh( v.clmatch / 2000 ), v.prse, v.e19, v.hclecl, v.prse19, v.prse49, v.sprd, v.prse4mx, v.prsm, v.spdm} );
}

double FutureNeutralIDTool::isNotH( const LHCb::Calo::Interfaces::INeutralID::Observables& v ) const {
  return m_reader1->GetMvaValue(
      {tanh( v.clmatch / 2000 ), v.prse, v.e19, v.hclecl, v.prse19, v.prse49, v.sprd, v.prse4mx, v.prsm, v.spdm} );
}
