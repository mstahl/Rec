/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREPIDS_CALOFUTURECHI22ID_H
#define CALOFUTUREPIDS_CALOFUTURECHI22ID_H 1

// Include files
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

// ============================================================================
/** @class CaloFutureChi22ID CaloFutureChi22ID.h
 *
 *
 *  @author Ivan BELYAEV
 *  @date   2006-06-18
 */
// ============================================================================
namespace LHCb::Calo {
  template <typename TABLEI, typename TABLEO>
  class Chi22ID : public Gaudi::Functional::Transformer<TABLEO( const LHCb::Tracks&, const TABLEI& )> {
  public:
    using base_type = Gaudi::Functional::Transformer<TABLEO( const LHCb::Tracks&, const TABLEI& )>;
    using KeyValue  = typename base_type::KeyValue;
    using base_type::debug;
    using base_type::msgLevel;
    using base_type::setProperty;

    // standard constructor
    Chi22ID( const std::string& name, ISvcLocator* pSvc );

    /// algorithm execution
    TABLEO operator()( const Tracks& tracks, const TABLEI& input ) const override;

  protected:
    Gaudi::Property<float>            m_large{this, "CutOff", 10000, "large value"};
    Gaudi::Property<std::vector<int>> m_type{this, "AcceptedType", {}, "Accepted tracks types"};

    mutable Gaudi::Accumulators::StatCounter<> m_nLinks{this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "#total tracks"};
  };
} // namespace LHCb::Calo
#endif // CALOFUTURECHI22ID_H
