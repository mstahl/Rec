/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrackMatchAlg.h"

// ============================================================================
/** @class FutureElectronMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================
namespace LHCb::Calo {
  using TABLE           = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
  using CALOFUTURETYPES = LHCb::CaloHypos;

  struct ElectronMatchAlg final : TrackMatchAlg<TABLE, CALOFUTURETYPES> {
    static_assert( std::is_base_of_v<LHCb::CaloFuture2Track::IHypoTrTable2D, TABLE>,
                   "Table must inherit from IHypoTrTable2D" );

    ElectronMatchAlg( const std::string& name, ISvcLocator* pSvc )
        : TrackMatchAlg<TABLE, CALOFUTURETYPES>( name, pSvc ) {
      updateHandleLocation( *this, "Calos", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "Electrons" ) );
      updateHandleLocation( *this, "Output", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ElectronMatch" ) );
      updateHandleLocation( *this, "Filter", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "InEcal" ) );

      setProperty( "Tool", "CaloFutureElectronMatch/ElectronMatchFuture" ).ignore();
      setProperty( "Threshold", "10000" ).ignore();
      // track types:
      setProperty( "AcceptedType",
                   Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                                LHCb::Track::Types::Ttrack ) )
          .ignore();
      setProperty( "TableSize", "1000" ).ignore();
    }
  };

  // ============================================================================
} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::ElectronMatchAlg, "FutureElectronMatchAlg" )
