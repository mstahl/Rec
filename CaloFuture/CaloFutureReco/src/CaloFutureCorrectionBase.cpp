/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "CaloFutureCorrectionBase.h"
#include "Event/ProtoParticle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureCorrectionBase
//
// 2010-05-07 : Olivier Deschamps
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( CaloFutureCorrectionBase )

CaloFutureCorrectionBase::CaloFutureCorrectionBase( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<CaloFutureCorrectionBase>( this );
  m_cmLoc = LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" );

  // allocate memory for m_params. params[area][type]
  CaloFutureCorrection::ParamVector         vect;
  CaloFutureCorrection::PrecalculatedParams precalculated;
  CaloFutureCorrection::CachedParameters    typePar_tuple = {CaloFutureCorrection::Empty, vect, precalculated};
  std::vector<CaloFutureCorrection::CachedParameters> vect_tuple( CaloFutureCorrection::nT, typePar_tuple );
  m_params.reserve( area_size ); // area_size=3 (outer, middle, inner)
  for ( auto area = 0; area < area_size; ++area ) m_params.push_back( vect_tuple );
}

StatusCode CaloFutureCorrectionBase::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

  // transform vector of accepted hypos
  m_hypos.clear();
  for ( const auto& hypo : m_hypos_ ) {
    if ( hypo <= (int)LHCb::CaloHypo::Hypothesis::Undefined || hypo >= (int)LHCb::CaloHypo::Hypothesis::Other ) {
      return Error( "Invalid/Unknown  Calorimeter hypothesis object!" );
    }
    m_hypos.push_back( LHCb::CaloHypo::Hypothesis( hypo ) );
  }

  // locate and set and configure the Detector
  m_det = getDet<DeCalorimeter>( m_detData );
  if ( !m_det ) { return StatusCode::FAILURE; }
  m_calo.setCaloFuture( m_detData );
  //
  if ( m_hypos.empty() ) return Error( "Empty vector of allowed Calorimeter Hypotheses!" );

  // debug printout of all allowed hypos
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << " List of allowed hypotheses : " << endmsg;
    for ( const auto& h : m_hypos ) { debug() << " -->" << h << endmsg; }
    for ( const auto& c : m_corrections ) { debug() << "Accepted corrections :  '" << c << "'" << endmsg; }
  }

  // get external tools
  counterStat = tool<IFutureCounterLevel>( "FutureCounterLevel" );
  return setConditionParams( m_conditionName );
}

StatusCode CaloFutureCorrectionBase::finalize() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Finalize" << endmsg;

  if ( m_corrections.size() > 1 || *( m_corrections.begin() ) != "All" ) {
    for ( const auto& c : m_corrections ) { info() << "Accepted corrections :  '" << c << "'" << endmsg; }
  }
  if ( m_corrections.empty() ) warning() << "All corrections have been disabled for " << name() << endmsg;

  if ( m_cond == nullptr )
    warning() << " Applied corrections configured via options for  " << name() << endmsg;
  else if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " Applied corrections configured via condDB  ('" << m_conditionName << "') for " << name() << endmsg;

  for ( auto area = 0; area < 3; ++area ) {
    for ( auto itype = 0; itype < static_cast<int>( CaloFutureCorrection::nT ); ++itype ) {
      int func = m_params[area][itype].function;
      if ( func == CaloFutureCorrection::Empty ) continue;
      const auto& type = CaloFutureCorrection::typeName[itype];
      const auto& vec  = m_params[area][itype].values;

      if ( !vec.empty() ) {

        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
          debug() << " o  '" << type << "'  correction as a '" << CaloFutureCorrection::funcName[func]
                  << "' function of " << vec.size() << " parameters" << endmsg;
        }
      } else {
        warning() << " o '" << type << "' correction HAS NOT BEEN APPLIED  (badly configured)" << endmsg;
      }
    }
  }

  m_hypos.clear();

  return GaudiTool::finalize(); // must be called after all other actions
}

//=============================================================================
StatusCode CaloFutureCorrectionBase::setDBParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Get params from CondDB condition = " << m_conditionName << endmsg;
  registerCondition( m_conditionName, m_cond, &CaloFutureCorrectionBase::updParams );
  return runUpdate();
}
// ============================================================================
StatusCode CaloFutureCorrectionBase::setOptParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Get params from options " << endmsg;
  if ( m_optParams.empty() && m_conditionName != "none" ) {
    info() << "No default options parameters defined" << endmsg;
    return StatusCode::SUCCESS;
  }
  for ( const auto& p : m_optParams ) {
    const std::string& name = p.first;
    if ( accept( name ) ) { Params( name, p.second ); }
  }
  checkParams();
  return StatusCode::SUCCESS;
}
// ============================================================================
StatusCode CaloFutureCorrectionBase::updParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "updParams() called" << endmsg;
  if ( !m_cond ) return Error( "Condition points to NULL", StatusCode::FAILURE );

  // toDo
  for ( const auto& paramName : m_cond->paramNames() ) {
    if ( m_cond->exists( paramName ) ) {
      const auto& params = m_cond->paramAsDoubleVect( paramName );
      if ( accept( paramName ) ) { Params( paramName, params ); }
    }
  }
  checkParams();

  return StatusCode::SUCCESS;
}
void CaloFutureCorrectionBase::Params( const std::string& paramName, const CaloFutureCorrection::ParamVector& params ) {
  // check if known type
  CaloFutureCorrection::Type type = stringToCorrectionType( paramName );
  bool                       ok   = false;
  for ( int itype = 0; itype < static_cast<int>( CaloFutureCorrection::lastType ); ++itype ) {
    if ( static_cast<int>( type ) == itype ) {
      ok = true;
      break;
    }
  }
  if ( !ok ) {
    warning() << " o Type " << paramName << " is not registered" << endmsg;
    return;
  }

  // const auto& active_par = Params( paramName, params );
  // if ( !active_par.active ) continue;

  // get parameters
  // const auto& pars = active_par.data;
  if ( params.size() < 2 ) return;
  for ( int area = 0; area <= 2; area++ ) {

    // consistency of pars checked elsewhere - straight parsing here
    const auto& func         = params[0];
    const auto& dim          = params[1];
    const int   step         = ( func != CaloFutureCorrection::GlobalParamList ) ? 3 : 1;
    const int   start_offset = ( func != CaloFutureCorrection::GlobalParamList ) ? area : 0;
    int         pos          = 2 + start_offset;

    if ( step * dim + 2 != static_cast<int>( params.size() ) ) {
      warning() << "o Size of DB parameter vector does not match the nominal value. : [ " << params << " ]" << endmsg;
      continue;
    }

    m_params[area][type].function = static_cast<CaloFutureCorrection::Function>( func );

    m_params[area][type].values.clear();
    m_params[area][type].values.reserve( dim );
    for ( int i = 0; i < dim; ++i ) {
      m_params[area][type].values.push_back( params[pos] );
      pos += step;
    }
    // precalculated exponent, sinh and cosh values: see 'polynomial functions' and 'Sshape function' in
    // CaloFutureCorrectionBase::getCorrection and CaloFutureCorrectionBase::getCorrectionDerivative
    if ( !m_params[area][type].values.empty() ) {
      const auto&      b                        = m_params[area][type].values[0];
      constexpr double delta                    = 0.5;
      m_params[area][type].precalculated.myexp  = myexp( b );
      auto sinh_val                             = ( b != 0 ) ? mysinh( delta / b ) : INFINITY;
      m_params[area][type].precalculated.mysinh = sinh_val;
      m_params[area][type].precalculated.mycosh = sqrt( 1. + sinh_val * sinh_val );
    }
  }
}

const CaloFutureCorrection::CachedParameters&
CaloFutureCorrectionBase::getParams( const CaloFutureCorrection::Type type, const LHCb::CaloCellID id ) const {
  auto area = id.area();

  return m_params[area][type];
}

std::optional<double> CaloFutureCorrectionBase::getCorrectionImpl( const CaloFutureCorrection::Type type,
                                                                   const LHCb::CaloCellID id, const double var,
                                                                   double* derivative ) const {
  bool   doDerivative = ( derivative != nullptr );
  double cor          = 0.;

  const auto& pars = getParams( type, id );
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    const auto& name = CaloFutureCorrection::typeName[type];
    debug() << "Correction type " << name << " to be applied on cluster (seed = " << id << ") is a '"
            << CaloFutureCorrection::funcName[pars.function] << "' function with params = " << pars.values << endmsg;
  }

  // compute correction
  if ( pars.function == CaloFutureCorrection::Unknown || pars.values.empty() ) return std::nullopt;

  // list accessor - not correction :
  if ( pars.function == CaloFutureCorrection::ParamList || pars.function == CaloFutureCorrection::GlobalParamList ) {
    warning() << " Param accessor is a fake function - no correction to be applied - return default value" << endmsg;
    return std::nullopt;
  }

  // polynomial correction
  const auto& temp = pars.values;
  // and precalculated exponents of some of these values
  auto& temp_precalc = pars.precalculated;

  // polynomial functions
  if ( pars.function == CaloFutureCorrection::Polynomial || pars.function == CaloFutureCorrection::InversPolynomial ||
       pars.function == CaloFutureCorrection::ExpPolynomial ||
       pars.function == CaloFutureCorrection::ReciprocalPolynomial ) {
    double v = 1.;
    cor      = 0.;
    for ( auto i = temp.begin(); i != temp.end(); ++i ) {
      cor += ( *i ) * v;
      if ( pars.function == CaloFutureCorrection::ReciprocalPolynomial )
        v = ( var == 0 ) ? 0. : v / var;
      else
        v *= var;
#if defined( __clang__ )
      // Without this, clang optimiser does something with this loop that causes FPE...
      if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) verbose() << "cor = " << cor << endmsg;
#endif
    }
    if ( pars.function == CaloFutureCorrection::InversPolynomial )
      if ( cor != 0 ) cor = 1. / cor;
    if ( pars.function == CaloFutureCorrection::ExpPolynomial ) {
      if ( cor != 0 ) {
        if ( var == 0.0 )
          // if var == 0.0 then cor == temp[0]
          cor = temp_precalc.myexp;
        else
          cor = myexp( cor );
      }
    }

    if ( doDerivative ) {
      if ( pars.function == CaloFutureCorrection::Polynomial ||
           pars.function == CaloFutureCorrection::InversPolynomial ||
           pars.function == CaloFutureCorrection::ExpPolynomial ) {
        *derivative = 0.;
        double v    = 1.;
        int    cnt  = 0;
        auto   i    = temp.begin();
        for ( ++i, cnt++; i != temp.end(); ++i, cnt++ ) {
          *derivative += ( *i ) * cnt * v;
          v *= var;
        }
        if ( pars.function == CaloFutureCorrection::InversPolynomial ) *derivative = -( *derivative ) * cor * cor;
        if ( pars.function == CaloFutureCorrection::ExpPolynomial ) *derivative = ( *derivative ) * cor;
      } else if ( pars.function == CaloFutureCorrection::ReciprocalPolynomial ) {
        *derivative = 0.;
        if ( var != 0 ) {
          auto v   = 1. / ( var * var );
          int  cnt = 0;
          auto i   = temp.begin();
          for ( ++i, cnt++; i != temp.end(); ++i, cnt++ ) {
            *derivative -= ( *i ) * cnt * v;
            v /= var;
          }
        }
      }
    }
  }

  // sigmoid function
  else if ( pars.function == CaloFutureCorrection::Sigmoid ) {
    if ( temp.size() == 4 ) {
      const auto& a          = temp[0];
      const auto& b          = temp[1];
      const auto& c          = temp[2];
      const auto& d          = temp[3];
      auto        mytanh_val = mytanh( c * ( var + d ) );
      cor                    = a + b * mytanh_val;
      if ( doDerivative ) *derivative = b * c * ( 1. - mytanh_val * mytanh_val );
    } else {
      Warning( "The power sigmoid function must have 4 parameters" ).ignore();
    }
  }

  // Sshape function
  else if ( pars.function == CaloFutureCorrection::Sshape || pars.function == CaloFutureCorrection::SshapeMod ) {
    if ( temp.size() == 1 ) {
      const auto&      b     = temp[0];
      constexpr double delta = 0.5;
      if ( b > 0 ) {
        double arg = var / delta;
        double csh = 1.;
        if ( pars.function == CaloFutureCorrection::SshapeMod ) {
          arg *= temp_precalc.mysinh;
          csh = temp_precalc.mysinh;
        } else if ( pars.function == CaloFutureCorrection::Sshape ) {
          arg *= temp_precalc.mycosh;
          csh = temp_precalc.mycosh;
        }
        cor = b * mylog( arg + std::sqrt( arg * arg + 1.0 ) );
        if ( doDerivative ) *derivative = b / delta * csh / std::sqrt( arg * arg + 1. );
      }
    } else {
      Warning( "The Sshape function must have 1 parameter" ).ignore();
    }
  }

  // Shower profile function
  else if ( pars.function == CaloFutureCorrection::ShowerProfile ) {
    if ( temp.size() == 10 ) {
      if ( var > 0.5 ) {
        auto tmp1 = temp[0] * myexp( -temp[1] * var );
        auto tmp2 = temp[2] * myexp( -temp[3] * var );
        auto tmp3 = temp[4] * myexp( -temp[5] * var );
        cor       = tmp1;
        cor += tmp2;
        cor += tmp3;
        if ( doDerivative ) {
          *derivative = -temp[1] * tmp1;
          *derivative += -temp[3] * tmp2;
          *derivative += -temp[5] * tmp3;
        }
      } else {
        cor       = 2.;
        auto tmp1 = temp[6] * myexp( -temp[7] * var );
        auto tmp2 = temp[8] * myexp( -temp[9] * var );
        cor -= tmp1;
        cor -= tmp2;
        if ( doDerivative ) {
          *derivative += temp[7] * tmp1;
          *derivative += temp[9] * tmp2;
        }
      }
    } else {
      Warning( "The ShowerProfile function must have 10 parameters" ).ignore();
    }
  }

  // Sinusoidal function
  else if ( pars.function == CaloFutureCorrection::Sinusoidal ) {
    if ( temp.size() == 1 ) {
      const double&    A       = temp[0];
      constexpr double twopi   = 2. * M_PI;
      const auto       sin_val = mysin( twopi * var );
      cor                      = A * sin_val;
      if ( doDerivative ) *derivative = A * twopi * sqrt( 1. - sin_val * sin_val );
    } else {
      Warning( "The Sinusoidal function must have 1 parameter" ).ignore();
    }
  }

  if ( counterStat->isVerbose() ) kounter( type, id.areaName() ) += cor;

  return cor;
}

void CaloFutureCorrectionBase::checkParams() {
  for ( int area = 0; area < 3; ++area ) {
    if ( static_cast<int>( m_params[area].size() ) != static_cast<int>( CaloFutureCorrection::nT ) ) {
      warning() << "Corrections vector size != " << CaloFutureCorrection::nT << endmsg;
    }

    for ( int itype = 0; itype < static_cast<int>( CaloFutureCorrection::nT ); ++itype ) {
      int  func = m_params[area][itype].function;
      bool ok   = true;
      if ( func != CaloFutureCorrection::Empty ) {
        if ( m_params[area][itype].values.size() == 0 ) {
          warning() << "Empty parameter vector of type " << CaloFutureCorrection::typeName[itype] << " of function "
                    << CaloFutureCorrection::funcName[func] << endmsg;
          ok = false;
        }
        if ( func >= CaloFutureCorrection::Unknown ) {
          warning() << " o Function for correction of type'" << CaloFutureCorrection::typeName[itype]
                    << "' is  not defined." << endmsg;
          ok = false;
        }
      }
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) && ok ) )
        debug() << " o Will apply correction '" << CaloFutureCorrection::funcName[itype] << "' as a '"
                << CaloFutureCorrection::funcName[func] << "' function of " << m_params[area][itype].values.size()
                << " parameters" << endmsg;
      if ( !ok ) {
        m_params[area][itype].values.clear();
        m_params[area][itype].function = CaloFutureCorrection::Empty;
      }
    }
  }
}
