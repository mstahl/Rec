/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
// Include files
// from GaudiKernel
#include "GaudiKernel/MsgStream.h"
// CaloFutureGen
#include "CaloKernel/CaloException.h"
// LHCb Kernel
#include "GaudiKernel/SystemOfUnits.h"
// CaloFutureEvent
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
// CaloFutureUtils
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloFutureUtils/Digit2ClustersConnector.h"
#include "CaloFutureUtils/SharedCells.h"
// local
#include "CaloFutureSharedCellAlg.h"

// ============================================================================
/** @file CaloFutureSharedCellAlg.cpp
 *
 *  Implementation file for class : CaloFutureSharedCellAlg
 *
 *  @see CaloFutureSharedCellAlg
 *  @see Digit2ClustersConnector
 *  @see SharedCells
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 30/06/2001
 */
// ============================================================================

DECLARE_COMPONENT( CaloFutureSharedCellAlg )

// ============================================================================
// Standard creator, initializes variables
// ============================================================================
CaloFutureSharedCellAlg::CaloFutureSharedCellAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  // set default data as a function of detector
  m_detData   = LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name );
  m_inputData = LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name );
}

// ============================================================================
// Main execution
// ============================================================================
StatusCode CaloFutureSharedCellAlg::execute() {
  // avoid long names
  using namespace SharedCells;
  using namespace LHCb::CaloDigitStatus;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Execute" << endmsg;

  // useful typedefs
  typedef LHCb::CaloClusters  Clusters;
  typedef const DeCalorimeter Detector;

  // locate input data
  Clusters* clusters = get<Clusters>( m_inputData );
  if ( 0 == clusters ) { return StatusCode::FAILURE; }

  // locate geometry (if needed)
  Detector* detector = nullptr;
  if ( m_useDistance ) {
    detector = getDet<DeCalorimeter>( m_detData );
    if ( !detector ) { return StatusCode::FAILURE; }
  }

  Clusters* output = nullptr;
  if ( m_copy ) ///< make a new container
  {
    output = new Clusters();
    put( output, m_outputData );
    // make a copy
    for ( const LHCb::CaloCluster* i : *clusters ) {
      if ( i ) { output->insert( new LHCb::CaloCluster{*i} ); }
    }
  } else {
    output = clusters;
  } ///< update existing sequence

  /** build inverse connection table/object
   *  keep only digits which have connections
   *   with 2 clusters and more!
   */
  const unsigned int      cutOff = 2;
  Digit2ClustersConnector table;
  StatusCode              scc = table.load( output->begin(), output->end(), cutOff );
  if ( scc.isFailure() ) warning() << "Unable to load the table" << endmsg;

  // sort digit by energy in order the subsequent processing is pointer address independent !
  const auto&                         map = table.map();
  std::vector<const LHCb::CaloDigit*> reOrder;
  reOrder.reserve( map.size() );
  std::transform( map.begin(), map.end(), std::back_inserter( reOrder ),
                  []( const auto& entry ) { return entry.first; } );
  std::stable_sort( reOrder.begin(), reOrder.end(),
                    LHCb::CaloDataFunctor::inverse( LHCb::CaloDataFunctor::Less_by_Energy ) );

  /// loop over all digits in the table
  for ( const auto& idig : reOrder ) {
    auto entry = table.map().find( idig );
    if ( entry == table.map().end() ) continue;
    const LHCb::CaloDigit* dig = entry->first;
    /// ignore  artificial zeros
    if ( !dig ) { continue; }

    StatusCode sc = StatusCode::SUCCESS;
    if ( m_useSumEnergy && !m_useDistance ) {
      sc = summedEnergyAlgorithm( entry->second, m_numIterations ); // do not apply weights at this stage
    } else if ( !m_useSumEnergy && !m_useDistance ) {
      sc = seedEnergyAlgorithm( entry->second, SeedCell );
    } else if ( m_useSumEnergy && m_useDistance ) {
      sc = summedDistanceAlgorithm( entry->second, detector, dig->cellID(), m_showerSizes, m_numIterations );
    } else if ( !m_useSumEnergy && m_useDistance ) {
      sc = seedDistanceAlgorithm( entry->second, detector, SeedCell, dig->cellID(), m_showerSizes );
    } else {
      return Error( "Funny condition :-)) " );
    }
    if ( sc.isFailure() ) { return Error( "Could not redistribute the energy!" ); }
  }

  m_clusterCount += clusters->size();

  return StatusCode::SUCCESS;
}
