/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
/** @file
 *
 *  Implementation file for class: ClassifyPhotonElectron
 *  The implementation is partially based on previous
 *  SinglePhotonAlg and ElectronAlg codes.
 *
 *  @author Carla Marin carla.marin@cern.ch
 *  @date   23/05/2019
 */
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "ICaloFutureHypoTool.h"
#include "Relations/RelationWeighted2D.h"
#include <string>

/** @class ClassifyPhotonElectron ClassifyPhotonElectron.h
 *
 *  Classification of electromagnetic clusters into photon and
 *  electron hypothesis according to track matching chi2.
 *  Energy and position corrections are applied to the resulting
 *  calo hypotheses.
 *  Implementation partially based on previous SinglePhotonAlg
 *  and ElectronAlg
 *
 *  @author Carla Marin carla.marin@cern.ch
 *  @date   23/05/2019
 */

namespace LHCb::Calo::Algorithm {

  using TrackMatchTable = RelationWeighted2D<CaloCluster, Track, float>;

  namespace {
    bool apply( const ToolHandleArray<Interfaces::IProcessHypos>& c, span<LHCb::CaloHypo* const> hypos,
                const TrackMatchTable* ctable = nullptr ) {
      return std::all_of( std::begin( c ), std::end( c ),
                          [&]( const auto& elem ) { return elem->correct( hypos, ctable ).isSuccess(); } );
    }
  } // namespace

  class ClassifyPhotonElectron
      : public Gaudi::Functional::MultiTransformer<
            std::tuple<CaloHypos, CaloHypos>( const DeCalorimeter&, const CaloClusters&, const TrackMatchTable& ),
            DetDesc::usesConditions<DeCalorimeter>> {
  public:
    ClassifyPhotonElectron( const std::string& name, ISvcLocator* pSvc );

    // returns 2 CaloHypos: photons and electrons respectively
    std::tuple<CaloHypos, CaloHypos> operator()( const DeCalorimeter&, const CaloClusters&,
                                                 const TrackMatchTable& ) const override;

  private:
    void printHypoDebugInfo( CaloHypo const& hypo, bool pass ) const;
    void printDebugInfo( const CaloCluster&                                       cluster,
                         CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const;
    bool validateCluster( const CaloCluster&                                       cluster,
                          CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const {
      int m = cluster.entries().size();
      return ( cluster.e() > m_ecut ) && ( eT( &cluster ) > m_eTcut ) && ( m > m_minDigits ) && ( m < m_maxDigits );
    }

    // Correction tools for now used as they were in
    // SinglePhotonAlg and ElectronAlg
    PublicToolHandleArray<Interfaces::IProcessHypos> m_correc_photon{this,
                                                                     "PhotonCorrectionTools",
                                                                     {
                                                                         "CaloFutureECorrection/ECorrectionPhoton",
                                                                         "CaloFutureSCorrection/SCorrectionPhoton",
                                                                         "CaloFutureLCorrection/LCorrectionPhoton",
                                                                     },
                                                                     "List of tools for 'fine-corrections' "};
    PublicToolHandleArray<Interfaces::IProcessHypos> m_correc_electr{this,
                                                                     "ElectronCorrectionTools",
                                                                     {
                                                                         "CaloFutureECorrection/ECorrectionElectron",
                                                                         "CaloFutureSCorrection/SCorrectionElectron",
                                                                         "CaloFutureLCorrection/LCorrectionElectron",
                                                                     },
                                                                     "List of tools for 'fine-corrections' "};

    // selection cuts
    Gaudi::Property<float>  m_ecut{this, "MinEnergy", 0., "Threshold on cluster energy"};
    Gaudi::Property<float>  m_eTcut{this, "MinET", 0., "Threshold on cluster transverse energy"};
    Gaudi::Property<int>    m_minDigits{this, "MinDigits", 0, "Threshold on minimum cluster digits"};
    Gaudi::Property<int>    m_maxDigits{this, "MaxDigits", 9999, "Threshold on maximum cluster digits"};
    Gaudi::Property<double> m_photonEtCut{this, "PhotonMinEt", 0., "Threshold on photon cluster & hypo ET"};
    Gaudi::Property<double> m_electrEtCut{this, "ElectrMinEt", 0., "Threshold on electron cluster & hypo ET"};
    Gaudi::Property<float>  m_photonChi2Cut{this, "PhotonMinChi2", 0.,
                                           "Threshold on minimum photon cluster track match chi2"};
    Gaudi::Property<float>  m_electrChi2Cut{this, "ElectrMaxChi2", 0.,
                                           "Threshold on maximum electron cluster track match chi2"};

    mutable Gaudi::Accumulators::StatCounter<>          m_counterPhotons{this, "photonHypos"};
    mutable Gaudi::Accumulators::StatCounter<>          m_counterElectrs{this, "electronHypos"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_errApply{this,
                                                                   "Error from Correction Tool - skip the cluster", 3};
  };

  DECLARE_COMPONENT_WITH_ID( ClassifyPhotonElectron, "ClassifyPhotonElectronAlg" )

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================
  ClassifyPhotonElectron::ClassifyPhotonElectron( const std::string& name, ISvcLocator* pSvc )
      : MultiTransformer( name, pSvc,
                          // Inputs
                          {
                              KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} ),
                              KeyValue( "InputClusters", {CaloClusterLocation::Ecal} ),
                              KeyValue( "InputTable", {CaloFutureIdLocation::ClusterMatch} ),
                          },
                          // Outputs
                          {KeyValue( "OutputPhotons", {CaloHypoLocation::Photons} ),
                           KeyValue( "OutputElectrons", {CaloHypoLocation::Electrons} )} ) {}

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  std::tuple<CaloHypos, CaloHypos> ClassifyPhotonElectron::
                                   operator()( const DeCalorimeter& calo, const CaloClusters& clusters, const TrackMatchTable& table ) const {
    // output containers
    auto result                = std::tuple<CaloHypos, CaloHypos>{};
    auto& [photons, electrons] = result;
    photons.reserve( clusters.size() );
    electrons.reserve( clusters.size() );

    // used in the loop
    auto eT = CaloDataFunctor::EnergyTransverse{&calo};

    auto hasTrackMatch = [&table]( const CaloCluster& cluster, double chi2Cut ) {
      return !( table.relations( &cluster, chi2Cut, false ) ).empty();
    };

    // loop on clusters
    for ( const auto* cl : clusters ) {
      if ( msgLevel( MSG::DEBUG ) ) printDebugInfo( *cl, eT );
      if ( validateCluster( *cl, eT ) ) {
        // 1. check if cluster fullfills photon hypo requirements
        if ( ( CaloMomentum( cl ).pt() >= m_photonEtCut ) && !hasTrackMatch( *cl, m_photonChi2Cut ) ) {
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " --> Cluster satisfies photon e, et, digits, pt and chi2 requirements" << endmsg;
          }

          auto photonHypo = std::make_unique<CaloHypo>();
          photonHypo->setHypothesis( CaloHypo::Hypothesis::Photon );
          photonHypo->addToClusters( cl );
          photonHypo->setPosition( std::make_unique<CaloPosition>( cl->position() ) );
          photons.insert( photonHypo.release() );
        }

        // 2. check if cluster fullfills electron hypo requirements
        if ( ( CaloMomentum( cl ).pt() >= m_electrEtCut ) && hasTrackMatch( *cl, m_electrChi2Cut ) ) {
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " --> Cluster satisfies electron e, et, digits, pt and chi2 requirements" << endmsg;
          }

          auto electronHypo = std::make_unique<CaloHypo>();
          electronHypo->setHypothesis( CaloHypo::Hypothesis::EmCharged );
          electronHypo->addToClusters( cl );
          electronHypo->setPosition( std::make_unique<CaloPosition>( cl->position() ) );
          electrons.insert( electronHypo.release() );
        }
      }
    }

    bool corr_photons_ok = apply( m_correc_photon, make_span( photons.begin(), photons.end() ) );
    bool corr_electrons_ok = apply( m_correc_electr, make_span( electrons.begin(), electrons.end() ), &table );
    if ( UNLIKELY( !corr_photons_ok ) || UNLIKELY( !corr_electrons_ok ) ) ++m_errApply;

    if ( msgLevel( MSG::DEBUG ) ) {
      std::for_each( photons.begin(), photons.end(),
                     [&]( const auto& hypo ) { printHypoDebugInfo( *hypo, CaloMomentum( hypo ).pt() >= m_eTcut ); } );
      std::for_each( electrons.begin(), electrons.end(), [&]( const auto& hypo ) {
        printHypoDebugInfo( *hypo, CaloMomentum( hypo ).pt() >= m_electrEtCut );
      } );
    }
    photons.erase( std::remove_if( photons.begin(), photons.end(),
                                   [&]( const auto& hypo ) { return CaloMomentum( hypo ).pt() < m_eTcut; } ),
                   photons.end() );
    electrons.erase( std::remove_if( electrons.begin(), electrons.end(),
                                     [&]( const auto& hypo ) { return CaloMomentum( hypo ).pt() < m_electrEtCut; } ),
                     electrons.end() );

    // debug info
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " # of created Photon Hypos is " << photons.size() << endmsg;
      debug() << " # of created Electron Hypos is " << electrons.size() << endmsg;
    }

    // counters
    m_counterPhotons += photons.size();
    m_counterElectrs += electrons.size();

    return result;
  }

  void ClassifyPhotonElectron::printDebugInfo( const CaloCluster&                                       cluster,
                                               CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const {
    debug() << "*Variables and cut values:" << endmsg;
    debug() << " - e: " << cluster.e() << " " << m_ecut << endmsg;
    debug() << " - eT:" << eT( &cluster ) << " " << m_eTcut << endmsg;
    debug() << " - pt:" << CaloMomentum( &cluster ).pt() << " " << m_photonEtCut << endmsg;
    debug() << " - m: " << cluster.entries().size() << " " << m_minDigits << " " << m_maxDigits << endmsg;
    debug() << " - chi2 cut: " << m_photonChi2Cut << endmsg;
  }

  void ClassifyPhotonElectron::printHypoDebugInfo( CaloHypo const& hypo, bool pass ) const {
    debug() << " - pt hypo: " << CaloMomentum( &hypo ).pt() << endmsg;
    debug() << " - pt cut: " << m_eTcut << endmsg;
    if ( !pass ) debug() << "DOES NOT PASS!" << endmsg;
  }

} // namespace LHCb::Calo::Algorithm
