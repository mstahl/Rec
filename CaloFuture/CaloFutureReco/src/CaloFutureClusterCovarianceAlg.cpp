/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from STL
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CovarianceEstimator.h"
#include "Event/CaloCluster.h"
#include "FutureSubClusterSelectorTool.h"
#include "ICaloFutureClusterTool.h"

/** @class CaloFutureClusterCovarianceAlg CaloFutureClusterCovarianceAlg.h
 *
 *   Simple algorithm for evaluation of covariance matrix
 *   for CaloFutureCluster object
 *
 *  @see CaloFutureClusterCovarianceAlg
 *  @see ICaloFutureClusterTool
 *  @see ICaloFutureSubClusterTag
 *
 *  @author Vanya Belyaev Ivan Belyaev
 *  @date   04/07/2001
 */

namespace LHCb::Calo::Algorithms {
  class ClusterCovariance
      : public Gaudi::Functional::Transformer<CaloCluster::Container( const CaloCluster::Container& )> {

  public:
    /** Standard constructor
     *  @param   name          algorithm name
     *  @param   pSvcLocator   pointer to Service Locator
     */
    ClusterCovariance( const std::string& name, ISvcLocator* pSvcLocator );

    /** standard execution method
     *  @see GaudiAlgorithm
     *  @see     Algorithm
     *  @see    IAlgorithm
     *  @return Container of
     */
    CaloCluster::Container operator()( const CaloCluster::Container& ) const override;

  private:
    // tool used for covariance matrix calculation
    ToolHandle<ICaloFutureClusterTool>       m_cov{this, "CovarianceTool",
                                             "FutureClusterCovarianceMatrixTool/" +
                                                 Utilities::CaloFutureNameFromAlg( name() ) + "CovarTool"};
    ToolHandle<FutureSubClusterSelectorTool> m_tagger{this, "TaggerTool",
                                                      "FutureSubClusterSelectorTool/" +
                                                          Utilities::CaloFutureNameFromAlg( name() ) + "ClusterTag"};
    // tool used for cluster spread estimation
    ToolHandle<ICaloFutureClusterTool> m_spread{
        this, "SpreadTool", "FutureClusterSpreadTool/" + Utilities::CaloFutureNameFromAlg( name() ) + "SpreadTool"};

    // following properties are inherited by the selector tool when defined:
    Gaudi::Property<std::string>              m_tagName{this, "TaggerName"};
    Gaudi::Property<std::vector<std::string>> m_taggerE{this, "EnergyTags"};
    Gaudi::Property<std::vector<std::string>> m_taggerP{this, "PositionTags"};

    // collection of known cluster shapes
    Gaudi::Property<std::map<std::string, std::vector<double>>> m_covParams{
        this, "CovarianceParameters", {}}; // KEEP IT UNSET ! INITIAL VALUE WOULD BYPASS DB ACCESS

    // counter
    mutable Gaudi::Accumulators::Counter<> m_clusters{this, "# clusters"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_cov_error{this, "Error from cov,    skip cluster "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_spread_error{this, "Error from spread, skip cluster "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_tagger_error{this, "Error from tagger, skip cluster "};
  };

  // ============================================================================
  /** Standard constructor
   *  @param   name          algorithm name
   *  @param   pSVcLocator   pointer to Service Locator
   */
  // ============================================================================
  ClusterCovariance::ClusterCovariance( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     KeyValue{"InputData", CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalOverlap" )},
                     KeyValue{"OutputData", CaloFutureAlgUtils::CaloFutureClusterLocation( name, "Ecal" )} ) {}

  // ===========================================================================
  CaloCluster::Container ClusterCovariance::operator()( const CaloCluster::Container& clusters ) const {

    // create new container for output clusters
    CaloCluster::Container outputClusters;
    // update the version number (needed for serialization)
    outputClusters.setVersion( 2 );
    outputClusters.reserve( clusters.size() );
    // copy clusters to new container, and apply corrections on the way...
    for ( const auto* in : clusters ) {
      if ( !in ) continue;

      auto out = std::make_unique<CaloCluster>( *in );

      // == APPLY TAGGER
      StatusCode sc = m_tagger->tag( *out );
      if ( sc.isFailure() ) {
        ++m_tagger_error;
        continue;
      }

      // == APPLY COVARIANCE ESTIMATOR
      sc = ( *m_cov )( *out );
      if ( sc.isFailure() ) {
        ++m_cov_error;
        continue;
      }

      // == APPLY SPREAD ESTIMATOR
      sc = ( *m_spread )( *out );
      if ( sc.isFailure() ) {
        ++m_spread_error;
        continue;
      }

      outputClusters.insert( out.release() );
    }

    // == counter
    m_clusters += clusters.size();

    return outputClusters;
  }
} // namespace LHCb::Calo::Algorithms

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Algorithms::ClusterCovariance, "CaloFutureClusterCovarianceAlg" )
