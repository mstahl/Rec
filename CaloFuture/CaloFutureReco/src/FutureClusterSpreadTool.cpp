/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Property.h"
// ============================================================================
// CaloFutureInterfaces
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
#include "ICaloFutureClusterTool.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// CaloFutureUtil
// ============================================================================
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/SpreadEstimator.h"
// ============================================================================
// Include files
// ============================================================================
/** @class FutureClusterSpreadTool FutureClusterSpreadTool.h
 *
 *  Concrete tool for estimation of the
 *  effective cluster size ("spread")
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   22/11/2001
 */
// ============================================================================
class FutureClusterSpreadTool : public extends<GaudiTool, ICaloFutureClusterTool> {
public:
  // ==========================================================================
  /** Standard constructor
   */
  using extends::extends;
  // ==========================================================================
  /** standard initialization method
   *  @return status code
   */
  StatusCode initialize() override;
  // ==========================================================================
  /** standard finalization method
   *  @return status code
   */
  StatusCode finalize() override;
  // ==========================================================================
  /** The main processing method (functor interface)
   *  @param cluster pointer to CaloCluster object to be processed
   *  @return status code
   */
  StatusCode operator()( LHCb::CaloCluster& cluster ) const override;
  // ==========================================================================
private:
  // ==========================================================================
  SpreadEstimator              m_estimator;
  Gaudi::Property<std::string> m_detData{this, "Detector", LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name() )};
  const DeCalorimeter*         m_det = nullptr;
  // ==========================================================================
};
// ============================================================================
/** @file FutureClusterSpreadTool.cpp
 *
 *  Implementation file for class : FutureClusterSpreadTool
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 23/11/2001
 */
// ============================================================================
DECLARE_COMPONENT( FutureClusterSpreadTool )
// ============================================================================
/*  standard initialization method
 *  @return status code
 */
// ============================================================================
StatusCode FutureClusterSpreadTool::initialize() {
  /// initialize the base class
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;
  ///
  m_det = getDet<DeCalorimeter>( m_detData );
  /// configure the estimator
  m_estimator.setDetector( m_det );
  ///
  return sc;
}
// ============================================================================
/*  standard finalization method
 *  @return status code
 */
// ============================================================================
StatusCode FutureClusterSpreadTool::finalize() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << " Corrected Clusters, Ratio : " << m_estimator.invalidRatio() << endmsg;
    debug() << " Corrected Clusters, Et    : " << m_estimator.invalidEnergy() << endmsg;
    debug() << " Corrected Clusters, Cells : " << m_estimator.invalidCells() << endmsg;
  }
  /// finalize the base class
  return extends::finalize();
}
// ============================================================================
/*  The main processing method (functor interface)
 *  @param cluster pointer to CaloCluster object to be processed
 *  @return status code
 */
// ============================================================================
StatusCode FutureClusterSpreadTool::operator()( LHCb::CaloCluster& cluster ) const {
  /// apply the estimator
  return m_estimator( &cluster );
}
// ============================================================================
// The END
// ============================================================================
