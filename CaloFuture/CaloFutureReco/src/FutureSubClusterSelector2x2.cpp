/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// Event
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
// CaloFutureUtils
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CellMatrix2x2.h"
#include "CaloFutureUtils/ClusterFunctors.h"
// local
#include "FutureSubClusterSelector2x2.h"

// ============================================================================
/** @file FutureSubClusterSelector2x2.cpp
 *
 *  Implementation file for class : FutureSubClusterSelector2x2
 *
 *  @author F. Machefert
 *  @date 06/14/2014
 */
// ============================================================================

DECLARE_COMPONENT( FutureSubClusterSelector2x2 )

// ============================================================================
/** standard initiliazation
 *  @return status code
 */
// ============================================================================
StatusCode FutureSubClusterSelector2x2::initialize() {
  /// initliaze the base class
  StatusCode sc = FutureSubClusterSelectorBase::initialize();
  if ( sc.isFailure() ) return sc;
  if ( !det() ) return Error( "DeCalorimeter* ponts to NULL!" );
  m_det = det();
  m_matrix.setDet( m_det );
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** The main processing method
 *  @param cluster pointer to CaloFutureCluster object to be processed
 *  @return status code
 */
// ============================================================================
StatusCode FutureSubClusterSelector2x2::tag( LHCb::CaloCluster& cluster ) const {
  double                       energy;
  CellMatrix2x2::SubMatrixType type;
  //
  StatusCode sc = choice2x2( &cluster, type, energy );
  if ( sc.isFailure() ) return sc;
  //
  m_matrix.setType( type );
  return LHCb::ClusterFunctors::tagTheSubCluster( &cluster, m_matrix, modify(), mask(),
                                                  LHCb::CaloDigitStatus::ModifiedByMax2x2Tagger );
}

// ============================================================================
/** The main processing method (untag)
 *  @param cluster pointer to CaloCluster object to be processed
 *  @return status code
 */
// ============================================================================
StatusCode FutureSubClusterSelector2x2::untag( LHCb::CaloCluster& cluster ) const {
  double                       energy;
  CellMatrix2x2::SubMatrixType type;
  //
  StatusCode sc = choice2x2( &cluster, type, energy );
  if ( sc.isFailure() ) return sc;
  //
  m_matrix.setType( type );
  return LHCb::ClusterFunctors::untagTheSubCluster( &cluster, m_matrix, LHCb::CaloDigitStatus::ModifiedByMax2x2Tagger );
}

// ============================================================================
/** The method that selects the 2x2 cluster out of the full cluster
 *  @param cluster pointer to CaloCluster object to be processed
 *  @param type the selected type of cluster
 *  @param energy the energy of the selected cluster
 *  @return status code
 */
// ============================================================================
StatusCode FutureSubClusterSelector2x2::choice2x2( LHCb::CaloCluster* cluster, CellMatrix2x2::SubMatrixType& type,
                                                   double& energy ) const {
  // check the arguments
  if ( !cluster ) { return Error( "cluster points to NULL in 'choice 2x2()'" ); }
  // get all entries
  const auto& entries = cluster->entries();
  // find seed digit
  auto seedEntry =
      LHCb::ClusterFunctors::locateDigit( entries.begin(), entries.end(), LHCb::CaloDigitStatus::SeedCell );
  // check the seed
  if ( entries.end() == seedEntry ) { return Error( "could not find the seed of the cluster in 'choice2x2()'." ); }
  const LHCb::CaloDigit* seed = seedEntry->digit();
  if ( !seed ) { return Error( "cluster seed points to NULL in 'choice2x2()'." ); }

  LHCb::CaloCellID seedCell = seed->cellID();

  // loop over all entried
  CellMatrix2x2 matrix( m_det );

  double         e[4];
  constexpr auto mode = std::array{CellMatrix2x2::UpperLeft, CellMatrix2x2::UpperRight, CellMatrix2x2::LowerLeft,
                                   CellMatrix2x2::LowerRight};

  for ( int j = 0; j < 4; ++j ) {
    matrix.setType( mode[j] );
    e[j] = std::accumulate( entries.begin(), entries.end(), 0., [&]( double e, const auto& entry ) {
      const LHCb::CaloDigit* digit = entry.digit();
      if ( !digit ) return e;
      double frac = matrix( seedCell, digit->cellID() );
      return e + frac * digit->e();
    } );
  }
  // select the max energy case
  type   = mode[0];
  energy = e[0];
  for ( int i = 1; i < 4; ++i ) {
    if ( e[i] > energy ) {
      energy = e[i];
      type   = mode[i];
    }
  }
  return StatusCode::SUCCESS;
}

// ============================================================================
// The End
// ============================================================================
