/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>
#include <array>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

namespace Rich::Future::Rec::MC::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class CherenkovResolution RichCherenkovResolution.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class CherenkovResolution final : public Consumer<void( const Summary::Track::Vector&,                   //
                                                          const LHCb::Track::Selection&,                   //
                                                          const SIMDPixelSummaries&,                       //
                                                          const Rich::PDPixelCluster::Vector&,             //
                                                          const Relations::PhotonToParents::Vector&,       //
                                                          const LHCb::RichTrackSegment::Vector&,           //
                                                          const CherenkovAngles::Vector&,                  //
                                                          const CherenkovResolutions::Vector&,             //
                                                          const SIMDCherenkovPhoton::Vector&,              //
                                                          const Rich::Future::MC::Relations::TkToMCPRels&, //
                                                          const LHCb::MCRichDigitSummarys& ),
                                                    Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    CherenkovResolution( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                     KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
      // print some stats on the final plots
      // setProperty ( "HistoPrint", true );
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Selection&                   tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const CherenkovResolutions::Vector&             ckResolutions, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Number of bins for 1D pull plots
    static const std::size_t nPullBins = 10;

    /// momentum bin
    inline std::size_t pullBin( const double ptot, const Rich::RadiatorType rad ) const {
      return ( ptot < m_PtotMin[rad]
                   ? 0
                   : ptot > m_PtotMax[rad] ? nPullBins - 1
                                           : ( std::size_t )( ( ptot - m_PtotMin[rad] ) * m_pullPtotInc[rad] ) );
    }

    /// min max for given momentum bin ( in GeV )
    std::pair<double, double> binMinMaxPtot( const std::size_t bin, const Rich::RadiatorType rad ) const {
      return {( m_PtotMin[rad] + ( bin / m_pullPtotInc[rad] ) ) / Gaudi::Units::GeV,
              ( m_PtotMin[rad] + ( ( bin + 1 ) / m_pullPtotInc[rad] ) ) / Gaudi::Units::GeV};
    }

    /// Get the histo ID string for a given bin
    inline std::string binToID( const std::size_t i ) const noexcept {
      return "Pulls/PtotBins/ckPull-Bin" + std::to_string( i );
    }

  private:
    /// Which radiators to monitor
    Gaudi::Property<RadiatorArray<bool>> m_rads{this, "Radiators", {false, true, true}};

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.0f, 0.0f, 0.0f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.005f, 0.008f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.055f, 0.031f}};

    /// Min for expected CK resolution
    Gaudi::Property<RadiatorArray<float>> m_ckResMin{this, "CKResMin", {0.0f, 0.00075f, 0.00040f}};

    /// Max for expected CK resolution
    Gaudi::Property<RadiatorArray<float>> m_ckResMax{this, "CKResMax", {0.025f, 0.0026f, 0.00102f}};

    /// Min momentum by radiator
    Gaudi::Property<RadiatorArray<double>> m_PtotMin{
        this, "PtotMin", {0.0, 2.0 * Gaudi::Units::GeV, 5.0 * Gaudi::Units::GeV}};

    /// Max momentum by radiator
    Gaudi::Property<RadiatorArray<double>> m_PtotMax{
        this, "PtotMax", {100.0 * Gaudi::Units::GeV, 100.0 * Gaudi::Units::GeV, 100.0 * Gaudi::Units::GeV}};

    /// Option to skip electrons
    Gaudi::Property<bool> m_skipElectrons{this, "SkipElectrons", false};

  private:
    // cached data

    /// 1/increment for pull ptot binning
    RadiatorArray<double> m_pullPtotInc = {0, 0, 0};

    // cached histogram pointers

    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_expCKang      = {{}};
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_ckres         = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_ckresVcktheta = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_ckresVptot    = {{}};

    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVckang     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVPtot      = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVckang = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVPtot  = {{}};

    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_ckPullVckang     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_ckPullVPtot      = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsCkPullVckang = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsCkPullVPtot  = {{}};
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode CherenkovResolution::prebookHistograms() {
  using namespace Gaudi::Units;

  bool ok = true;

  // Loop over radiators
  for ( const auto rad : Rich::radiators() ) {

    // cache some numbers for later use
    assert( m_PtotMax[rad] > m_PtotMin[rad] );
    m_pullPtotInc[rad] = nPullBins / ( m_PtotMax[rad] - m_PtotMin[rad] );

    if ( m_rads[rad] ) {

      // Loop over all particle codes
      for ( const auto hypo : Rich::particles() ) {
        // always skip Below Threshold type
        if ( hypo != Rich::BelowThreshold ) {
          // track plots
          ok &= saveAndCheck( h_expCKang[rad][hypo], richHisto1D( HID( "expCKang", rad, hypo ), "Expected CK angle",
                                                                  m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_ckres[rad][hypo], richHisto1D( HID( "ckres", rad, hypo ), "Calculated CKres",
                                                               m_ckResMin[rad], m_ckResMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_ckresVcktheta[rad][hypo],
                              richProfile1D( HID( "ckresVcktheta", rad, hypo ), "Calculated CKres V CKangle",
                                             m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_ckresVptot[rad][hypo],
                              richProfile1D( HID( "ckresVptot", rad, hypo ), "Calculated CKres V ptot", m_PtotMin[rad],
                                             m_PtotMax[rad], nBins1D() ) );
          // photon plots
          ok &= saveAndCheck( h_diffckVckang[rad][hypo], //
                              richProfile1D( HID( "diffckVckang", rad, hypo ),
                                             "Rec-Exp CKtheta V CKtheta - MC true photons", m_ckThetaMin[rad],
                                             m_ckThetaMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_diffckVPtot[rad][hypo], richProfile1D( HID( "diffckVPtot", rad, hypo ),
                                                                       "Rec-Exp CKtheta V ptot - MC true photons",
                                                                       m_PtotMin[rad], m_PtotMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_fabsdiffckVckang[rad][hypo],
                              richProfile1D( HID( "fabsdiffckVckang", rad, hypo ),
                                             "fabs(Rec-Exp) CKtheta V CKtheta - MC true photons", m_ckThetaMin[rad],
                                             m_ckThetaMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_fabsdiffckVPtot[rad][hypo],
                              richProfile1D( HID( "fabsdiffckVPtot", rad, hypo ),
                                             "fabs(Rec-Exp) CKtheta V ptot - MC true photons", m_PtotMin[rad],
                                             m_PtotMax[rad], nBins1D() ) );
          // pull plots
          ok &= saveAndCheck( h_ckPullVckang[rad][hypo],
                              richProfile1D( HID( "ckPullVckang", rad, hypo ),
                                             "(Rec-Exp)/Res CKtheta V CKtheta - MC true photons", m_ckThetaMin[rad],
                                             m_ckThetaMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_ckPullVPtot[rad][hypo], richProfile1D( HID( "ckPullVPtot", rad, hypo ),
                                                                       "(Rec-Exp)/Res CKtheta V ptot - MC true photons",
                                                                       m_PtotMin[rad], m_PtotMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_fabsCkPullVckang[rad][hypo],
                              richProfile1D( HID( "fabsCkPullVckang", rad, hypo ),
                                             "fabs((Rec-Exp)/Res) CKtheta V CKtheta - MC true photons",
                                             m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() ) );
          ok &= saveAndCheck( h_fabsCkPullVPtot[rad][hypo],
                              richProfile1D( HID( "fabsCkPullVPtot", rad, hypo ),
                                             "fabs((Rec-Exp)/Res) CKtheta V ptot - MC true photons", //
                                             m_PtotMin[rad], m_PtotMax[rad], nBins1D() ) );
          // 1D plots binned in ptot
          for ( std::size_t i = 0; i < nPullBins; ++i ) {
            // min/max Ptot for this bin
            const auto binEdges = binMinMaxPtot( i, rad );
            // hist title string
            std::ostringstream title;
            title << "(Rec-Exp)/Res CKtheta - MC true photons - Bin " << i << " Ptot " << binEdges.first << " to "
                  << binEdges.second << " GeV";
            // book the histo
            richHisto1D( HID( binToID( i ), rad, hypo ), title.str(), -5, 5, nBins1D() );
          }
        }
      }
    }
  }

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void CherenkovResolution::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                      const LHCb::Track::Selection&                   tracks,        //
                                      const SIMDPixelSummaries&                       pixels,        //
                                      const Rich::PDPixelCluster::Vector&             clusters,      //
                                      const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                      const LHCb::RichTrackSegment::Vector&           segments,      //
                                      const CherenkovAngles::Vector&                  expTkCKThetas, //
                                      const CherenkovResolutions::Vector&             ckResolutions, //
                                      const SIMDCherenkovPhoton::Vector&              photons,       //
                                      const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                      const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // loop over the track info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {
    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !m_rads[rad] ) continue;

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // get the resolutions for this segment
      const auto& ckRes = ckResolutions[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles( *tk, true, 0.5 );

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // loop over mass hypos
      for ( const auto hypo : activeParticlesNoBT() ) {
        if ( expCKangles[hypo] > 0 ) {
          h_expCKang[rad][hypo]->fill( expCKangles[hypo] );
          h_ckres[rad][hypo]->fill( ckRes[hypo] );
          h_ckresVcktheta[rad][hypo]->fill( expCKangles[hypo], ckRes[hypo] );
          h_ckresVptot[rad][hypo]->fill( pTot, ckRes[hypo] );
        }
      }

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) continue;

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];

        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, skip
          if ( Rich::Unknown == pid ) continue;
          // skip electrons which are reconstructed badly..
          if ( m_skipElectrons && Rich::Electron == pid ) continue;

          // beta cut for true MC type
          const auto mcbeta = richPartProps()->beta( pTot, pid );
          if ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] ) {

            // true Cherenkov signal ?
            const bool trueCKSig = std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

            // expected CK theta ( for true type )
            const auto thetaExp = expCKangles[pid];

            // delta theta
            const auto deltaTheta = thetaRec - thetaExp;

            // fill some plots for true photons only
            if ( trueCKSig ) {
              h_diffckVckang[rad][pid]->fill( thetaExp, deltaTheta, mcPW );
              h_diffckVPtot[rad][pid]->fill( pTot, deltaTheta, mcPW );
              h_fabsdiffckVckang[rad][pid]->fill( thetaExp, fabs( deltaTheta ), mcPW );
              h_fabsdiffckVPtot[rad][pid]->fill( pTot, fabs( deltaTheta ), mcPW );

              // pulls
              if ( ckRes[pid] > 0 ) {
                const auto pull = deltaTheta / ckRes[pid];
                richHisto1D( HID( binToID( pullBin( pTot, rad ) ), rad, pid ) )->fill( pull, mcPW );
                h_ckPullVckang[rad][pid]->fill( thetaExp, pull, mcPW );
                h_ckPullVPtot[rad][pid]->fill( pTot, pull, mcPW );
                h_fabsCkPullVckang[rad][pid]->fill( thetaExp, fabs( pull ), mcPW );
                h_fabsCkPullVPtot[rad][pid]->fill( pTot, fabs( pull ), mcPW );
              }
            }

          } // beta selection

        } // loop over associated MCPs

      } // scalar loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CherenkovResolution )

//-----------------------------------------------------------------------------
