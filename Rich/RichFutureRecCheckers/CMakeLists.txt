###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RichFutureRecCheckers
################################################################################

gaudi_subdir(RichFutureRecCheckers v1r0)

gaudi_depends_on_subdirs(Event/MCEvent
                         Kernel/MCInterfaces
                         Kernel/Relations
                         Tr/TrackInterfaces
                         Rich/RichUtils
                         Rich/RichFutureRecInterfaces
                         Rich/RichFutureRecBase
                         Rich/RichFutureMCUtils)

find_package(ROOT)
find_package(Boost)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} 
                           ${Vc_INCLUDE_DIR})

gaudi_add_module(RichFutureRecCheckers
                 src/*.cpp
                 INCLUDE_DIRS AIDA Boost Event/MCEvent Kernel/Relations Kernel/MCInterfaces Tr/TrackInterfaces Rich/RichUtils Rich/RichFutureRecInterfaces
                 LINK_LIBRARIES RelationsLib MCEvent RichUtils RichFutureRecBase RichFutureMCUtilsLib)

# Fixes for GCC7.
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "6.99")
  set_property(TARGET RichFutureRecCheckers APPEND PROPERTY COMPILE_FLAGS " -faligned-new ")
endif()
