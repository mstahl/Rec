###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RichRecTests
################################################################################
gaudi_subdir(RichRecTests v1r0)

gaudi_depends_on_subdirs(Det/RichDet
                         GaudiKernel
                         Kernel/LHCbKernel
                         Kernel/LHCbMath
                         Rich/RichUtils
                         Rich/RichRecUtils)

find_package(Boost)
find_package(GSL)
find_package(Eigen)
find_package(Vc)
find_package(VDT)
find_package(ROOT COMPONENTS MathCore GenVector )

# flags to turn off AVX512. 
set(NO_AVX512_FLAGS     "-mno-avx512bw -mno-avx512cd -mno-avx512dq -mno-avx512er -mno-avx512f -mno-avx512ifma -mno-avx512pf -mno-avx512vbmi -mno-avx512vl -mno-avx512vpopcntdq ")
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "7.99")
  set(NO_AVX512_FLAGS "${NO_AVX512_FLAGS} -mno-avx512vbmi2 -mno-avx512vnni ")
endif()
# Determine compiler flags for each build target
set(SSE_BUILD_FLAGS     " -msse4.2 -mno-avx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
set(AVX_BUILD_FLAGS     " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
set(AVX2_BUILD_FLAGS    " -mavx2 -mno-fma ${NO_AVX512_FLAGS}")
set(AVX2FMA_BUILD_FLAGS " -mavx2 -mfma ${NO_AVX512_FLAGS}")
# only use 'basic' avx512 options here..
set(AVX512_BUILD_FLAGS  " -mavx512f -mavx512cd -mavx512dq ")
exec_program(${CMAKE_CXX_COMPILER} ARGS -print-prog-name=as OUTPUT_VARIABLE _as)
if(NOT _as)
  message(ERROR "Could not find the 'as' assembler...")
else()
  exec_program(${_as} ARGS --version OUTPUT_VARIABLE _as_version)
  string(REGEX REPLACE "\\([^\\)]*\\)" "" _as_version "${_as_version}")
  string(REGEX MATCH "[1-9]\\.[0-9]+(\\.[0-9]+)?" _as_version "${_as_version}")
  if(_as_version VERSION_LESS "2.21.0")
     message(WARNING "binutils is too old to support AVX2+FMA... Falling back to AVX only.")
     set(AVX2_BUILD_FLAGS    " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
     set(AVX2FMA_BUILD_FLAGS " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
     set(AVX512_BUILD_FLAGS  " -mavx -mno-avx2 -mno-fma ${NO_AVX512_FLAGS}" )
  endif()
endif()

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${EIGEN_INCLUDE_DIRS} ${Vc_INCLUDE_DIR})

if(GAUDI_BUILD_TESTS)



  gaudi_add_executable(RichMirrorCompareTestSSE4
                       src/MirrorCompare/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichMirrorCompareTestSSE4 "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${SSE_BUILD_FLAGS} )  

  gaudi_add_executable(RichMirrorCompareTestAVX
                       src/MirrorCompare/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichMirrorCompareTestAVX "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX_BUILD_FLAGS} )  

  gaudi_add_executable(RichMirrorCompareTestAVX2
                       src/MirrorCompare/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichMirrorCompareTestAVX2 "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} ) 

  gaudi_add_executable(RichMirrorCompareTestAVX2FMA
                       src/MirrorCompare/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichMirrorCompareTestAVX2FMA "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} )    

  gaudi_add_executable(RichMirrorCompareTestAVX512
                       src/MirrorCompare/main_avx512.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichMirrorCompareTestAVX512 "-lrt ${Vc_LIB_DIR}/libVc.a" )
  set_property(SOURCE src/MirrorCompare/main_avx512.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX512_BUILD_FLAGS} )  




  gaudi_add_executable(RichPhotonRecoTestSSE4
                       src/PhotonReco/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichPhotonRecoTestSSE4 -lrt )
  set_property(SOURCE src/PhotonReco/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${SSE_BUILD_FLAGS} )  

  gaudi_add_executable(RichPhotonRecoTestAVX
                       src/PhotonReco/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichPhotonRecoTestAVX -lrt )
  set_property(SOURCE src/PhotonReco/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX_BUILD_FLAGS} )  

  gaudi_add_executable(RichPhotonRecoTestAVX2
                       src/PhotonReco/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichPhotonRecoTestAVX2 -lrt )
  set_property(SOURCE src/PhotonReco/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} ) 

  gaudi_add_executable(RichPhotonRecoTestAVX2FMA
                       src/PhotonReco/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichPhotonRecoTestAVX2FMA -lrt )
  set_property(SOURCE src/PhotonReco/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} ) 

  gaudi_add_executable(RichPhotonRecoTestAVX512
                       src/PhotonReco/main_avx512.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichPhotonRecoTestAVX512 -lrt )
  set_property(SOURCE src/PhotonReco/main_avx512.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX512_BUILD_FLAGS} )




  gaudi_add_executable(RichRayTracingTestSSE4
                       src/RayTracing/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichRayTracingTestSSE4 -lrt )
  set_property(SOURCE src/RayTracing/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-ignored-attributes ${SSE_BUILD_FLAGS}" )

  gaudi_add_executable(RichRayTracingTestAVX
                       src/RayTracing/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichRayTracingTestAVX -lrt )
  set_property(SOURCE src/RayTracing/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-ignored-attributes ${AVX_BUILD_FLAGS}" )

  gaudi_add_executable(RichRayTracingTestAVX2
                       src/RayTracing/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichRayTracingTestAVX2 -lrt )
  set_property(SOURCE src/RayTracing/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} )

  gaudi_add_executable(RichRayTracingTestAVX2FMA
                       src/RayTracing/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichRayTracingTestAVX2FMA -lrt )
  set_property(SOURCE src/RayTracing/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} )

  gaudi_add_executable(RichRayTracingTestAVX512
                       src/RayTracing/main_avx512.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichRayTracingTestAVX512 -lrt )
  set_property(SOURCE src/RayTracing/main_avx512.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX512_BUILD_FLAGS} )




  gaudi_add_executable(RichSIMDTestSSE4
                       src/SIMD/main_sse4.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichSIMDTestSSE4 -lrt )
  set_property(SOURCE src/SIMD/main_sse4.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-ignored-attributes ${SSE_BUILD_FLAGS}" )

  gaudi_add_executable(RichSIMDTestAVX
                       src/SIMD/main_avx.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichSIMDTestAVX -lrt )
  set_property(SOURCE src/SIMD/main_avx.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-ignored-attributes ${AVX_BUILD_FLAGS}" )

  gaudi_add_executable(RichSIMDTestAVX2
                       src/SIMD/main_avx2.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichSIMDTestAVX2 -lrt )
  set_property(SOURCE src/SIMD/main_avx2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2_BUILD_FLAGS} )

  gaudi_add_executable(RichSIMDTestAVX2FMA
                       src/SIMD/main_avx2fma.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichSIMDTestAVX2FMA -lrt )
  set_property(SOURCE src/SIMD/main_avx2fma.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX2FMA_BUILD_FLAGS} )

  gaudi_add_executable(RichSIMDTestAVX512
                       src/SIMD/main_avx512.cpp
                       INCLUDE_DIRS ROOT Vc Eigen GSL VDT src
                       LINK_LIBRARIES LHCbMathLib GSL GaudiKernel RichUtils )
  target_link_libraries( RichSIMDTestAVX512 -lrt )
  set_property(SOURCE src/SIMD/main_avx512.cpp APPEND_STRING PROPERTY COMPILE_FLAGS ${AVX512_BUILD_FLAGS} )



endif()
