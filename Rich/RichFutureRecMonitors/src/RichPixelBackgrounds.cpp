/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <array>
#include <mutex>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PixelBackgrounds RichPixelBackgrounds.h
   *
   *  Monitors the RICH pixel backgrounds.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class PixelBackgrounds final : public Consumer<void( const SIMDPixelSummaries&, //
                                                       const SIMDPixelBackgrounds& ),
                                                 Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PixelBackgrounds( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"PixelBackgroundsLocation", SIMDPixelBackgroundsLocation::Default}} ) {
      // Default number of bins
      setProperty( "NBins1DHistos", 25 );
      setProperty( "NBins2DHistos", 20 );
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Functional operator
    void operator()( const SIMDPixelSummaries&   pixels, //
                     const SIMDPixelBackgrounds& backgrounds ) const override {
      // the lock
      std::lock_guard lock( m_updateLock );

      for ( const auto&& [pix, bkg] : Ranges::ConstZip( pixels, backgrounds ) ) {
        // which rich
        const auto rich = pix.rich();

        // Loop over the scalar entries for this SIMD pixel
        for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
          // pixel background
          h_pixBkg[rich]->fill( bkg[i] );
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      // The max background value for each RICH for plots
      DetectorArray<float> maxBkg = {0.6, 0.3};

      for ( const auto rich : Rich::detectors() ) {
        ok &= saveAndCheck( h_pixBkg[rich], richHisto1D( HID( "pixBkg", rich ), "Pixel Likelihood Background", //
                                                         -0.001, maxBkg[rich], nBins1D() ) );
      }

      return StatusCode{ok};
    }

  private:
    /// Scalar type for SIMD data
    using FP = SIMD::DefaultScalarFP;
    /// SIMD floating point type
    using SIMDFP = SIMD::FP<Rich::SIMD::DefaultScalarFP>;

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;
    /// Background histogram
    DetectorArray<AIDA::IHistogram1D*> h_pixBkg = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PixelBackgrounds )

} // namespace Rich::Future::Rec::Moni
