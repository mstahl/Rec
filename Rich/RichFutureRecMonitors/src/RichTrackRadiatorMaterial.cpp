/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"

// DetDesc
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"

// Detector parameters
#include "RichRecUtils/RichDetParams.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class TrackRadiatorMaterial RichTrackRadiatorMaterial.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class TrackRadiatorMaterial final
      : public Consumer<void( const LHCb::RichTrackSegment::Vector& ), Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    TrackRadiatorMaterial( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, //
                    {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default}} ) {
      // print some stats on the final plots
      // setProperty ( "HistoPrint", true );
      // change some defaults.
      setProperty( "NBins1DHistos", 100 );
      setProperty( "NBins2DHistos", 100 );
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector& segments ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Which radiators to monitor
    Gaudi::Property<RadiatorArray<bool>> m_rads{this, "Radiators", {false, true, true}};

    /// Transport Service
    ServiceHandle<ITransportSvc> m_transSvc{this, "TransportSvc", "TransportSvc"};

    /// radiation length computation error
    mutable WarningCounter m_radLwarn{this, "Problem computing radiation length"};

  private:
    // cached histograms

    RadiatorArray<AIDA::IHistogram1D*> h_EffL                = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_EffLOvPathL         = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_PathL               = {{}};
    RadiatorArray<AIDA::IHistogram2D*> h_EffLVPathL          = {{}};
    RadiatorArray<AIDA::IProfile2D*>   h_EffLVEntryXY        = {{}};
    RadiatorArray<AIDA::IProfile2D*>   h_EffLVExitXY         = {{}};
    RadiatorArray<AIDA::IProfile2D*>   h_EffLOvPathLVEntryXY = {{}};
    RadiatorArray<AIDA::IProfile2D*>   h_EffLOvPathLVExitXY  = {{}};
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------

StatusCode TrackRadiatorMaterial::prebookHistograms() {

  RADIATOR_GLOBAL_POSITIONS_X;
  RADIATOR_GLOBAL_POSITIONS_Y;

  bool ok = true;

  // Loop over radiators
  for ( const auto rad : Rich::radiators() ) {
    if ( m_rads[rad] ) {

      // max path length
      const auto minL = ( Rich::Rich2Gas != rad ? 800.0 : 1400.0 );
      const auto maxL = ( Rich::Rich2Gas != rad ? 1100.0 : 3200.0 );

      ok &= saveAndCheck( h_EffL[rad], richHisto1D( HID( "EffL", rad ), "Effective length", 0, 0.3, nBins1D() ) );

      ok &= saveAndCheck( h_EffLOvPathL[rad],
                          richHisto1D( HID( "EffLOvPathL", rad ), "Effective length / unit pathlength", //
                                       0, 0.0001, nBins1D() ) );

      ok &= saveAndCheck( h_PathL[rad], richHisto1D( HID( "PathL", rad ), "Track pathlength", minL, maxL, nBins1D() ) );

      ok &= saveAndCheck( h_EffLVPathL[rad],
                          richHisto2D( HID( "EffLVPathL", rad ), "Effective length in rad units V Segment pathlength",
                                       minL, maxL, nBins2D(), 0, 0.3, nBins2D() ) );

      ok &= saveAndCheck( h_EffLVEntryXY[rad],                                                         //
                          richProfile2D( HID( "EffLVEntryXY", rad ), "Effective length V Entry (x,y)", //
                                         -xRadEntGlo[rad], xRadEntGlo[rad], nBins1D(),                 //
                                         -yRadEntGlo[rad], yRadEntGlo[rad], nBins1D() ) );

      ok &= saveAndCheck( h_EffLVExitXY[rad],                                                        //
                          richProfile2D( HID( "EffLVExitXY", rad ), "Effective length V Exit (x,y)", //
                                         -xRadExitGlo[rad], xRadExitGlo[rad], nBins1D(),             //
                                         -yRadExitGlo[rad], yRadExitGlo[rad], nBins1D() ) );

      ok &= saveAndCheck( h_EffLOvPathLVEntryXY[rad],                                         //
                          richProfile2D( HID( "EffLOvPathLVEntryXY", rad ),                   //
                                         "Effective length  / unit pathlength V Entry (x,y)", //
                                         -xRadEntGlo[rad], xRadEntGlo[rad], nBins1D(),        //
                                         -yRadEntGlo[rad], yRadEntGlo[rad], nBins1D() ) );

      ok &= saveAndCheck( h_EffLOvPathLVExitXY[rad],                                         //
                          richProfile2D( HID( "EffLOvPathLVExitXY", rad ),                   //
                                         "Effective length  / unit pathlength V Exit (x,y)", //
                                         -xRadExitGlo[rad], xRadExitGlo[rad], nBins1D(),     //
                                         -yRadExitGlo[rad], yRadExitGlo[rad], nBins1D() ) );
    }
  }

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void TrackRadiatorMaterial::operator()( const LHCb::RichTrackSegment::Vector& segments ) const {

  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();

  // loop over segments
  for ( const auto& seg : segments ) {

    // Which radiator
    const auto rad = seg.radiator();
    // which rich
    const auto rich = seg.rich();

    // radiator points
    // move the entry/exit points by +-10mm to move away from mirror structures etc.
    const auto entP = seg.entryPoint() + Gaudi::XYZVector( 0, 0, 10 );
    const auto extP = seg.exitPoint() + Gaudi::XYZVector( 0, 0, -10 );

    // select segments away from edges etc.
    if ( ( Rich::Rich1 == rich && ( extP.rho() < 70 || entP.rho() < 70 ) ) ||
         ( Rich::Rich2 == rich && ( extP.rho() < 300 || entP.rho() < 300 || //
                                    fabs( entP.x() ) > 2200 || fabs( extP.x() ) > 3200 ) ) ) {
      continue;
    }

    // path length (using the exact points used below)
    const auto length = std::sqrt( ( extP - entP ).mag2() );

    // get the radiation length
    try {
      const auto effL    = m_transSvc->distanceInRadUnits_r( entP, extP, tsCache, 0, nullptr );
      const auto effLOvL = ( length > 0 ? effL / length : 0.0 );

      // fill plots
      h_EffL[rad]->fill( effL );
      h_EffLOvPathL[rad]->fill( effLOvL );
      h_EffLVPathL[rad]->fill( length, effL );
      h_PathL[rad]->fill( length );
      h_EffLVEntryXY[rad]->fill( entP.x(), entP.y(), effL );
      h_EffLVExitXY[rad]->fill( extP.x(), extP.y(), effL );
      h_EffLOvPathLVEntryXY[rad]->fill( entP.x(), entP.y(), effLOvL );
      h_EffLOvPathLVExitXY[rad]->fill( extP.x(), extP.y(), effLOvL );
    } catch ( const TransportSvcException& excpt ) {
      ++m_radLwarn;
      _ri_debug << excpt.message() << endmsg;
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackRadiatorMaterial )

//-----------------------------------------------------------------------------
