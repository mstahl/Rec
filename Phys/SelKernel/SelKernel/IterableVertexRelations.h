/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrZip.h"
#include "SelKernel/VertexRelation.h"

namespace Sel::VertexRelations {
  /** Proxy type for iterating over BestVertexRelations objects. */
  DECLARE_PROXY( Proxy ) {
    PROXY_METHODS( dType, unwrap, BestVertexRelations, m_rels );
    auto bestPV() const { return m_rels->bestPV<dType, unwrap>( this->offset() ); }
  };
} // namespace Sel::VertexRelations

REGISTER_PROXY( BestVertexRelations, Sel::VertexRelations::Proxy );
REGISTER_HEADER( BestVertexRelations, "SelKernel/IterableVertexRelations.h" );