###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
gaudi_subdir(FunctorCore v1r0)

gaudi_depends_on_subdirs(Kernel/LHCbMath
                         Event/RecEvent
                         Event/TrackEvent
                         Tr/TrackKernel
                         Phys/SelKernel
                         Phys/SelTools
                         Pr/PrKernel
			 Kernel/PartProp)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(FunctorCoreLib
                  src/*.cpp
                  PUBLIC_HEADERS Functors
                  LINK_LIBRARIES LHCbMathLib TrackEvent TrackKernel)

gaudi_add_module(FunctorCore
                 src/Components/*.cpp
                 LINK_LIBRARIES FunctorCoreLib)

gaudi_add_unit_test(TestFunctors
                    tests/src/TestFunctors.cpp
                    LINK_LIBRARIES LHCbMathLib FunctorCoreLib
                    TYPE Boost)

gaudi_install_python_modules()
gaudi_add_test(python COMMAND nosetests --with-doctest -v ${CMAKE_CURRENT_SOURCE_DIR}/python)
gaudi_add_test(QMTest QMTEST)
