#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from Functors import PT, ISMUON, MINIPCUT
from Functors import math as fmath
from GaudiKernel.SystemOfUnits import *


def not_empty(f):
    assert len(f.code()) > 0 and len(f.headers()) > 0


def check(f):
    not_empty(f)
    print(f.code(), f.headers())


def test_basic_grammar():
    ipcut = MINIPCUT(IPCut=1.0, Vertices='Test/Path')
    f1 = ISMUON | ipcut
    f2 = ISMUON & ipcut
    f3 = ipcut | ISMUON
    f4 = ipcut & ISMUON
    f5 = ~ISMUON
    f6 = ~ipcut
    check(f2 | ~ISMUON)
    check(~ISMUON | f2)
    not_empty(f1)
    not_empty(f2)
    not_empty(f3)
    not_empty(f4)
    not_empty(f5)
    not_empty(f6)


def test_arithmetric():
    check(30 * GeV < PT)
    check(PT > 30 * GeV)
    check(PT >= 30 * GeV)
    check(PT <= 30 * GeV)
    check(PT < 30 * GeV)
    check(PT == 30 * GeV)
    check(PT != 30 * GeV)
    check(PT + ISMUON > 15 * GeV)


def test_functions():
    check(fmath.log(PT / GeV) > 1.0)
