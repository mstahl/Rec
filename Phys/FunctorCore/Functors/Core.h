/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Gaudi/Accumulators.h"
#include "Gaudi/PluginService.h"
#include "Kernel/SynchronizedValue.h"
#include <typeinfo>

/** @file Core.h
 *  @brief Definition of the type-erased Functor<Output(Input)> type
 */
namespace Gaudi {
  class Algorithm;
} // namespace Gaudi

/** @namespace Functors
 *
 *  Namespace for all new-style functors
 */
namespace Functors {
  /** @class Functor
   *  @brief Empty declaration of Type-erased wrapper around a generic functor
   *
   *  This is specialised as Functors::Functor<OutputType(InputType)>.
   */
  template <typename>
  class Functor;

  /** @class AnyFunctor
   *  @brief Type-agnostic base class for functors, used in IFunctorFactory.
   *  @todo  Could consider using std::any's techniques for type-checking.
   */
  struct AnyFunctor {
    virtual ~AnyFunctor()                                                    = default;
    virtual                       operator bool() const                      = 0;
    virtual void                  bind( Gaudi::Algorithm* owning_algorithm ) = 0;
    virtual std::type_info const& rtype() const                              = 0;
  };

  struct TopLevelInfo {
    Gaudi::Algorithm* m_algorithm{nullptr};
    auto              algorithm() const { return m_algorithm; }
    void              bind( Gaudi::Algorithm* alg ) { m_algorithm = alg; }
    void              Warning( std::string_view message, std::size_t count = 1 ) const;

  private:
    using MsgMap             = std::map<std::string, Gaudi::Accumulators::MsgCounter<MSG::WARNING>, std::less<>>;
    using SynchronizedMsgMap = LHCb::cxx::SynchronizedValue<MsgMap>;
    // We have to use std::unique_ptr here to make sure that this structure,
    // and the functors that hold it, remain moveable. SynchronizedValue
    // contains an std::mutex, which is not moveable.
    mutable std::unique_ptr<SynchronizedMsgMap> m_msg_counters{std::make_unique<SynchronizedMsgMap>()};
  };

  namespace detail {
    /** Helper to determine if the given type has a bind() method */
    template <typename T, typename Algorithm>
    using has_bind_ = decltype( std::declval<T>().bind( std::declval<Algorithm*>() ) );

    template <typename T, typename Algorithm>
    inline constexpr bool has_bind_v = Gaudi::cpp17::is_detected_v<has_bind_, T, Algorithm>;

    /** Call obj.bind( alg ) if obj has an appropriate bind method, otherwise do nothing. */
    template <typename Obj, typename Algorithm>
    void bind( Obj& obj, [[maybe_unused]] Algorithm* alg ) {
      if constexpr ( has_bind_v<Obj, Algorithm> ) { obj.bind( alg ); }
    }

    /** Helper to determine if the given type has a prepare() method
     */
    template <typename T>
    using has_prepare_ = decltype( std::declval<T>().prepare() );

    template <typename T>
    inline constexpr bool has_prepare_v = Gaudi::cpp17::is_detected_v<has_prepare_, T>;

    /** Helper to deterimine if the given type has a
     *  prepare( TopLevelInfo const& ) method.
     */
    template <typename T>
    using has_prepare_TopLevelInfo_ = decltype( std::declval<T>().prepare( std::declval<TopLevelInfo>() ) );

    template <typename T>
    inline constexpr bool has_prepare_TopLevelInfo_v = Gaudi::cpp17::is_detected_v<has_prepare_TopLevelInfo_, T>;

    /** TODO deprecate this? */
    template <typename Functor>
    auto prepare( Functor const& f /* event_context */ ) {
      if constexpr ( has_prepare_v<Functor> ) {
        return f.prepare( /* event_context */ );
      } else {
        return std::cref( f );
      }
    }

    template <typename Functor>
    auto prepare( Functor const& f /* evt_context */, TopLevelInfo const& top_level ) {
      if constexpr ( has_prepare_TopLevelInfo_v<Functor> ) {
        return f.prepare( /* evt_context */ top_level );
      } else if constexpr ( has_prepare_v<Functor> ) {
        return f.prepare( /* evt_context */ );
      } else {
        return std::cref( f );
      }
    }

    template <typename>
    struct is_variant : std::false_type {};

    template <typename... Ts>
    struct is_variant<std::variant<Ts...>> : std::true_type {};

    template <typename... Variants>
    inline constexpr bool are_all_variants_v = ( is_variant<std::decay_t<Variants>>::value && ... );
  } // namespace detail

  /** @brief Type-erased wrapper around a generic functor.
   *
   *  This is a std::function-like type-erasing wrapper that is used to store
   *  and pass around functors without exposing the extremely verbose full types
   *  of [composite] functors. This is specialised as
   *  Functors::Functor<OutputType(InputType)>, which is rather similar to
   *  std::function<Output(Input)>, but includes the extra bind( ... ) method
   *  that is needed to set up functors' data dependencies and associate them
   *  with the owning algorithm.
   *
   *  @todo Consider adding some local storage so we can avoid dynamic allocation
   *        for "small" functors.
   */
  template <typename OutputType, typename... InputType>
  class Functor<OutputType( InputType... )> final : public AnyFunctor {
  public:
    using result_type   = OutputType;
    using prepared_type = std::function<OutputType( InputType... )>;

  private:
    struct IFunctor {
      virtual ~IFunctor()                                                                 = default;
      virtual void                  bind( Gaudi::Algorithm* )                             = 0;
      virtual result_type           operator()( TopLevelInfo const&, InputType... ) const = 0;
      virtual prepared_type         prepare( TopLevelInfo const& ) const                  = 0;
      virtual std::type_info const& rtype() const                                         = 0;
    };

    template <typename F>
    struct FunctorImpl : public IFunctor {
      FunctorImpl( F f ) : m_f( std::move( f ) ) {}
      void        bind( Gaudi::Algorithm* alg ) override { detail::bind( m_f, alg ); }
      result_type operator()( /* evt_context */ TopLevelInfo const& top_level, InputType... input ) const override {
        // Prepare and call in one go, avoiding the overhead of prepared_type
        auto f = detail::prepare( m_f /* evt_context */, top_level );
        // If the arguments are std::variant, use the prepared functor as a
        // visitor, otherwise just call it
        if constexpr ( detail::are_all_variants_v<InputType...> ) {
          return std::visit( std::move( f ), input... );
        } else {
          return std::invoke( std::move( f ), input... );
        }
      }
      prepared_type prepare( /* evt_context */ TopLevelInfo const& top_level ) const override {
        auto f = detail::prepare( m_f /* evt_context */, top_level );
        if constexpr ( detail::are_all_variants_v<InputType...> ) {
          return [f = std::move( f )]( auto&&... x ) { return std::visit( f, x... ); };
        } else {
          return f;
        }
      }
      std::type_info const& rtype() const override {
        using prepared_t = decltype( detail::prepare( m_f /* evt_context */, std::declval<TopLevelInfo>() ) );
        if constexpr ( detail::are_all_variants_v<InputType...> ) {
          return typeid( decltype( std::visit( std::declval<prepared_t>(), std::declval<InputType>()... ) ) );
        } else {
          return typeid( std::invoke_result_t<prepared_t, InputType...> );
        }
      }

    private:
      F m_f;
    };
    std::unique_ptr<IFunctor> m_functor;
    TopLevelInfo              m_top_level;

  public:
    /** @brief Construct an empty Functor object.
     *
     *  Note that an empty Functor will throw an exception if the function call
     *  operator is called.
     */
    Functor() = default;

    /** Move constructor
     */
    Functor( Functor&& other ) = default;

    /** Move assignment
     */
    Functor& operator=( Functor&& other ) = default;

    /** Construct a filled Functor object
     */
    template <typename F>
    Functor( F func ) : m_functor( std::make_unique<FunctorImpl<F>>( std::move( func ) ) ) {}

    /** Fill this Functor with new contents.
     */
    template <typename F>
    Functor& operator=( F func ) {
      m_functor = std::make_unique<FunctorImpl<F>>( std::move( func ) );
      return *this;
    }

    /** @brief Bind the contained object to the given algorithm.
     *
     *  The contained object may have data dependencies, which must be bound to the algorithm
     *  that owns the functor. Calling a functor with data dependencies without binding it to
     *  an algorithm will cause an exception to be thrown.
     *
     *  @param owning_algorithm The algorithm to which the data dependency should be associated.
     *
     *  @todo Change the argument type to the minimal type needed to initialise a
     *        DataObjectReadHandle, Gaudi::Algorithm is over the top.
     */
    void bind( Gaudi::Algorithm* owning_algorithm ) override {
      m_functor->bind( owning_algorithm );
      m_top_level.bind( owning_algorithm );
    }

    /** @brief Invoke the contained object.
     *
     * Invoke the contained object. If the Functor object is empty, or if the contained object
     * has data dependencies and has not been bound to an owning algorithm, an exception will
     * be thrown.
     *
     * @param input Input to the contained object
     * @return      Output of the contained object
     */
    result_type operator()( InputType... input ) const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> called" ); }
      return std::invoke( *m_functor, m_top_level, input... );
    }

    prepared_type prepare() const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> prepared" ); }
      auto prepared = m_functor->prepare( m_top_level );
      if ( UNLIKELY( !prepared ) ) {
        throw std::runtime_error( "Functor<Out(In...)>::prepare() yielded an empty functor." );
      }
      return prepared;
    }

    /** Query whether the Functor is empty or not.
     *
     *  @return false if the Functor is empty, true if it is not
     */
    operator bool() const override { return bool{m_functor}; }

    /** Query the return type of the contained functor.
     *
     *  This is *not* supposed to be typeid( OutputType ), rather it is the
     *  typeid of the functor return type *before* that was converted to
     *  OutputType.
     */
    std::type_info const& rtype() const override {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> return type queried" ); }
      return m_functor->rtype();
    }

    /** This is useful for the functor cache.
     *
     * @todo Give a more useful description...
     */
    using Factory = typename ::Gaudi::PluginService::Factory<AnyFunctor*()>;
  };
} // namespace Functors
