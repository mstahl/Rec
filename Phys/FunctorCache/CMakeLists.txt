###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
gaudi_subdir(FunctorCache)

gaudi_depends_on_subdirs(Phys/FunctorCore
                         Phys/SelAlgorithms)

# Suppress compilation warnings from external packages
find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

# Import the cache creation module
include(LoKiFunctorsCache)

# if(TARGET) protection allows build in satellite (lb-dev) projects
# Make sure the InstantiateFunctors__ algorithms are available
if(TARGET SelAlgorithmsConf)
  list(APPEND conf_deps SelAlgorithmsConf)
endif()

# Also make sure that FunctorFactory is available
if(TARGET FunctorCoreConfUserDB)
  list(APPEND conf_deps FunctorCoreConfUserDB)
endif()

if(conf_deps)
  list(INSERT conf_deps 0 DEPENDS)
endif()

# Only actually build a functor cache if it is explicitly requested
if(THOR_BUILD_TEST_FUNCTOR_CACHE)
  # Disable LoKi-specific hacks in LoKiFunctorsCachePostActionOpts.py
  # For now there is no need for a ThOr-specific alternative.
  set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS_TMP ${LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS})
  set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS "")

  loki_functors_cache(FunctorTestCache
                      options/DisableLoKiCacheFunctors.py
		      options/SilenceErrors.py
		      options/SuppressLogMessages.py
                      ${Rec_SOURCE_DIR}/Phys/FunctorCore/tests/options/test_functors.py
                      FACTORIES FunctorFactory
                      LINK_LIBRARIES FunctorCoreLib
                      ${conf_deps}
                      SPLIT 75)

  # Restore the old value
  set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS ${LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS_TMP})
endif(THOR_BUILD_TEST_FUNCTOR_CACHE)