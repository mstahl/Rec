/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "ChargedProtoANNPIDCommonBase.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// Redirect streams
#include "Kernel/STLOStreamRedirect.h"

// STL
#include <algorithm>
#include <optional>

// boost
#include "boost/algorithm/string.hpp"
#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/regex.hpp"

//=============================================================================
// Constructor initialisation
//=============================================================================
template <class PBASE>
void ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::initCommonConstructor() {
  // nothing to do yet ...
}

//=============================================================================
// Initialisation
//=============================================================================
template <class PBASE>
StatusCode ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::initialize() {
  // Execute the base class initialize
  auto sc = PBASE::initialize();
  if ( !sc ) { return this->Error( "Failed to initialise Gaudi Base class", sc ); }

  // Printout from initialize
  if ( this->msgLevel( MSG::DEBUG ) ) { this->debug() << "Initialize" << endmsg; }

  // Setup incident services
  auto incSvc = this->template svc<IIncidentSvc>( "IncidentSvc", true );
  incSvc->addListener( this, IncidentType::EndEvent );
  if ( sc ) sc = this->releaseSvc( incSvc );

  // Avoid auto-loading of state provider
  m_trStateP.disable();

  return sc;
}
//=============================================================================

//=============================================================================
// Incident handle
//=============================================================================
template <class PBASE>
void ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::handle( const Incident& ) {
  m_summary = nullptr;
  m_odin    = nullptr;
}
//=============================================================================

//=============================================================================
// Finalisation
//=============================================================================
template <class PBASE>
StatusCode ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::finalize() {
  // Printout from finalization
  if ( this->msgLevel( MSG::DEBUG ) ) { this->debug() << "Finalize" << endmsg; }

  // Finalise base class and return
  return PBASE::finalize();
}
//=============================================================================

//=============================================================================
// Access the RecSummary
//=============================================================================
template <class PBASE>
const LHCb::RecSummary* ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::recSummary() const {
  if ( UNLIKELY( !m_summary ) ) {
    m_summary = this->template getIfExists<LHCb::RecSummary>( m_recSumPath, false );
    if ( UNLIKELY( !m_summary ) ) { m_summary = this->template getIfExists<LHCb::RecSummary>( m_recSumPath, true ); }
    // if still not found, issue a warning
    if ( UNLIKELY( !m_summary ) ) { this->Warning( "RecSummary missing at " + m_recSumPath ).ignore(); }
  }
  return m_summary;
}
//=============================================================================

//=============================================================================
// Access the ODIN
//=============================================================================
template <class PBASE>
const LHCb::ODIN* ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::odin() const {
  if ( UNLIKELY( !m_odin ) ) {
    m_odin = this->template getIfExists<LHCb::ODIN>( m_odinPath, false );
    if ( UNLIKELY( !m_odin ) ) { m_odin = this->template getIfExists<LHCb::ODIN>( m_odinPath, true ); }
    // if still not found, issue a warning
    if ( UNLIKELY( !m_odin ) ) { this->Warning( "ODIN missing at " + m_odinPath ).ignore(); }
  }
  return m_odin;
}
//=============================================================================

//=============================================================================
template <class PBASE>
typename ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::Input::ConstVector
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::getInputs( const StringInputs& names ) const {
  typename Input::ConstVector inputs;
  inputs.reserve( names.size() );
  for ( const auto& name : names ) { inputs.emplace_back( getInput( name ) ); }
  return inputs;
}
//=============================================================================

//=============================================================================
template <class PBASE>
typename ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::Input::SmartPtr
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::getInput( const std::string& name ) const {
  Input* input = nullptr;

  // Tracking variables
  if ( "TrackP" == name ) {
    input = new InTrackP();
  } else if ( "TrackPt" == name ) {
    input = new InTrackPt();
  } else if ( "TrackLikelihood" == name ) {
    input = new InTrackLikelihood();
  } else if ( "TrackGhostProbability" == name ) {
    input = new InTrackGhostProb();
  } else if ( "TrackCloneDist" == name ) {
    input = new InTrackCloneDist();
  } else if ( "TrackFitMatchChi2" == name ) {
    input = new InTrackExInfo( LHCb::Track::AdditionalInfo::FitMatchChi2 );
  } else if ( "TrackFitVeloChi2" == name ) {
    input = new InTrackExInfo( LHCb::Track::AdditionalInfo::FitVeloChi2 );
  } else if ( "TrackFitVeloNDoF" == name ) {
    input = new InTrackExInfo( LHCb::Track::AdditionalInfo::FitVeloNDoF );
  } else if ( "TrackFitTChi2" == name ) {
    input = new InTrackExInfo( LHCb::Track::AdditionalInfo::FitTChi2 );
  } else if ( "TrackFitTNDoF" == name ) {
    input = new InTrackExInfo( LHCb::Track::AdditionalInfo::FitTNDoF );
  } else if ( "TrackMatchChi2" == name ) {
    input = new InTrackExInfo( LHCb::Track::AdditionalInfo::MatchChi2 );
  } else if ( "TrackDOCA" == name ) {
    input = new InTrackDOCA();
  } else if ( "TrackVertexX" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::X, InTrackXYZ::Where::Vertex, this, -9999 );
  } else if ( "TrackVertexY" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Y, InTrackXYZ::Where::Vertex, this, -9999 );
  } else if ( "TrackVertexZ" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Z, InTrackXYZ::Where::Vertex, this, -9999 );
  } else if ( "TrackRich1EntryX" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::X, InTrackXYZ::Where::RICH1Entry, this, -9999 );
  } else if ( "TrackRich1EntryY" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Y, InTrackXYZ::Where::RICH1Entry, this, -9999 );
  } else if ( "TrackRich1EntryZ" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Z, InTrackXYZ::Where::RICH1Entry, this, -9999 );
  } else if ( "TrackRich2EntryX" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::X, InTrackXYZ::Where::RICH2Entry, this, -9999 );
  } else if ( "TrackRich2EntryY" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Y, InTrackXYZ::Where::RICH2Entry, this, -9999 );
  } else if ( "TrackRich2EntryZ" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Z, InTrackXYZ::Where::RICH2Entry, this, -9999 );
  } else if ( "TrackRich1ExitX" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::X, InTrackXYZ::Where::RICH1Exit, this, -9999 );
  } else if ( "TrackRich1ExitY" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Y, InTrackXYZ::Where::RICH1Exit, this, -9999 );
  } else if ( "TrackRich1ExitZ" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Z, InTrackXYZ::Where::RICH1Exit, this, -9999 );
  } else if ( "TrackRich2ExitX" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::X, InTrackXYZ::Where::RICH2Exit, this, -9999 );
  } else if ( "TrackRich2ExitY" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Y, InTrackXYZ::Where::RICH2Exit, this, -9999 );
  } else if ( "TrackRich2ExitZ" == name ) {
    input = new InTrackXYZ( InTrackXYZ::Coord::Z, InTrackXYZ::Where::RICH2Exit, this, -9999 );
  }

  // Rich Variables
  else if ( "RichUsedAero" == name ) {
    input = new InRichUsedAerogel();
  } else if ( "RichUsedR1Gas" == name ) {
    input = new InRichUsedR1Gas();
  } else if ( "RichUsedR2Gas" == name ) {
    input = new InRichUsedR2Gas();
  } else if ( "RichAboveElThres" == name ) {
    input = new InRichAboveElThres();
  } else if ( "RichAboveMuThres" == name ) {
    input = new InRichAboveMuThres();
  } else if ( "RichAbovePiThres" == name ) {
    input = new InRichAbovePiThres();
  } else if ( "RichAboveKaThres" == name ) {
    input = new InRichAboveKaThres();
  } else if ( "RichAbovePrThres" == name ) {
    input = new InRichAbovePrThres();
  } else if ( "RichAboveDeThres" == name ) {
    input = new InRichAboveDeThres();
  } else if ( "RichDLLe" == name ) {
    input = new InRichDLL( Rich::Electron );
  } else if ( "RichDLLmu" == name ) {
    input = new InRichDLL( Rich::Muon );
  } else if ( "RichDLLpi" == name ) {
    input = new InRichDLL( Rich::Pion );
  } else if ( "RichDLLk" == name ) {
    input = new InRichDLL( Rich::Kaon );
  } else if ( "RichDLLp" == name ) {
    input = new InRichDLL( Rich::Proton );
  } else if ( "RichDLLd" == name ) {
    input = new InRichDLL( Rich::Deuteron );
  } else if ( "RichDLLbt" == name ) {
    input = new InRichDLL( Rich::BelowThreshold );
  }

  // Muon variables
  else if ( "MuonIsLooseMuon" == name ) {
    input = new InMuonIsMuonLoose();
  } else if ( "MuonIsMuon" == name ) {
    input = new InMuonIsMuon();
  } else if ( "MuonNShared" == name ) {
    input = new InMuonNShared();
  } else if ( "MuonMuLL" == name ) {
    input = new InMuonLLMu();
  } else if ( "MuonBkgLL" == name ) {
    input = new InMuonLLBkg();
  } else if ( "MuonMVA1" == name ) {
    input = new InMuonMVA1();
  } else if ( "MuonMVA2" == name ) {
    input = new InMuonMVA2();
  } else if ( "MuonMVA3" == name ) {
    input = new InMuonMVA3();
  } else if ( "MuonMVA4" == name ) {
    input = new InMuonMVA4();
  } else if ( "MuonChi2Corr" == name ) {
    input = new InMuonChi2Corr();
  }

  // GEC Variables
  else if ( "NumProtoParticles" == name ) {
    input = new InNumProtos();
  } else if ( "NumCaloHypos" == name ) {
    input = new InNumCaloHypos();
  } else if ( "NumLongTracks" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nLongTracks, this );
  } else if ( "NumDownstreamTracks" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nDownstreamTracks, this );
  } else if ( "NumUpstreamTracks" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nUpstreamTracks, this );
  } else if ( "NumVeloTracks" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nVeloTracks, this );
  } else if ( "NumTTracks" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nTTracks, this );
  } else if ( "NumGhosts" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nGhosts, this );
  } else if ( "NumMuonTracks" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nMuonTracks, this );
  } else if ( "NumPVs" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nPVs, this );
  } else if ( "NumRich1Hits" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nRich1Hits, this );
  } else if ( "NumRich2Hits" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nRich2Hits, this );
  } else if ( "NumVeloClusters" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nVeloClusters, this );
  } else if ( "NumITClusters" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nITClusters, this );
  } else if ( "NumTTClusters" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nTTClusters, this );
  } else if ( "NumOTClusters" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nOTClusters, this );
  } else if ( "NumSPDHits" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nSPDhits, this );
  } else if ( "NumMuonCoordsS0" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nMuonCoordsS0, this );
  } else if ( "NumMuonCoordsS1" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nMuonCoordsS1, this );
  } else if ( "NumMuonCoordsS2" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nMuonCoordsS2, this );
  } else if ( "NumMuonCoordsS3" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nMuonCoordsS3, this );
  } else if ( "NumMuonCoordsS4" == name ) {
    input = new InRecSummary( LHCb::RecSummary::DataTypes::nMuonCoordsS4, this );
  }

  // Proto Extra Info variables with specific default requirements, not -999
  else if ( "InAccMuon" == name ) {
    input = new InProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccMuon, 0 );
  } else if ( "InAccEcal" == name ) {
    input = new InProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccEcal, 0 );
  } else if ( "InAccHcal" == name ) {
    input = new InProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccHcal, 0 );
  } else if ( "InAccPrs" == name ) {
    input = new InProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccPrs, 0 );
  } else if ( "InAccSpd" == name ) {
    input = new InProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccSpd, 0 );
  } else if ( "InAccBrem" == name ) {
    input = new InProtoExInfo( LHCb::ProtoParticle::additionalInfo::InAccBrem, 0 );
  } else if ( "CaloEcalChi2" == name ) {
    input = new InCaloEcalChi2();
  } else if ( "CaloBremChi2" == name ) {
    input = new InCaloBremChi2();
  } else if ( "CaloClusChi2" == name ) {
    input = new InCaloClusChi2();
  }

  // ODIN information
  else if ( "RunNumber" == name ) {
    input = new InODINRunNumber( this );
  } else if ( "EventNumber" == name ) {
    input = new InODINEventNumber( this );
  }

  // Generic ProtoParticle Extra Info
  else {
    input = new InProtoExInfo( LHCb::ProtoParticle::convertExtraInfo( name ) );
  }

  if ( !input ) {
    this->Exception( "Failed to create input object for '" + name + "'" );
  } else {
    input->setName( name );
  }

  return typename Input::SmartPtr( input );
}
//=============================================================================

//=============================================================================
// Cut constructor
//=============================================================================
template <class PBASE>
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::Cut::Cut( const std::string&                         desc,
                                                             const ChargedProtoANNPIDCommonBase<PBASE>* parent )
    : m_desc( desc ) {
  // Cuts must have a precise form. Either
  //    variable > value
  // or
  //    variable < value

  // Parse the cut string
  boost::regex                 re( "\\s+" );
  boost::sregex_token_iterator i( desc.begin(), desc.end(), re, -1 );
  boost::sregex_token_iterator j;
  std::vector<std::string>     matches;
  while ( i != j ) { matches.push_back( *i++ ); }
  if ( matches.size() == 3 ) {
    // Get the variable from its name
    m_variable = parent->getInput( matches[0] );

    // Delimitor
    m_OK = setDelim( matches[1] );

    // The cut value
    m_cut = boost::lexical_cast<double>( matches[2] );
  }

  // Remove spaces from the cached description string
  boost::erase_all( m_desc, " " );
}

#ifdef _ENABLE_NEUROBAYES
//=============================================================================
// Get ANN output for NeuroBayes network helper
//=============================================================================
template <class PBASE>
double
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::NeuroBayesANN::getOutput( const LHCb::ProtoParticle* proto ) const {
  // Fill the array of network inputs
  const auto& in = this->inputs();
  std::transform( in.begin(), in.end(), m_vars.begin(),
                  [&proto]( const auto& input ) { return input->value( proto ); }; );

  // FPE Guard for NB call
  FPE::Guard guard( true );

  // NeuroBayes seems to sporadically send mysterious std messages which we
  // cannot control... So forcibly intercept them all here and send to /dev/null
  std::optional<STL::OStreamRedirect> red;
  if ( m_suppressPrintout ) red.emplace();

  // return the NN output, rescaled to the range 0 to 1
  return 0.5 * ( 1.0 + (double)m_expert->nb_expert( m_vars.data() ) );
}
#endif

//=============================================================================
// Get ANN output for TMVA Reader helper
//=============================================================================
template <class PBASE>
double
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::TMVAReaderANN::getOutput( const LHCb::ProtoParticle* proto ) const {
  // Fill the array of network inputs
  const auto& ins = this->inputs();
  std::transform( ins.begin(), ins.end(), m_vars.begin(),
                  [&proto]( const auto& input ) { return input->value( proto ); } );

  // get the output and return
  return m_reader->EvaluateMVA( "PID" );
}

//=============================================================================
// Get ANN output for TMVA Imp helper
//=============================================================================
template <class PBASE>
double
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::TMVAImpANN::getOutput( const LHCb::ProtoParticle* proto ) const {
  // Fill the array of network inputs
  const auto& ins = this->inputs();
  std::transform( ins.begin(), ins.end(), m_vars.begin(),
                  [&proto]( const auto& input ) { return input->value( proto ); } );

  // FPE Guard for MVA call
  FPE::Guard guard( true );

  // get the output response and return
  return m_reader->GetMvaValue( m_vars );
}

//=============================================================================
// Get ANN output for yPID Imp helper
//=============================================================================
template <class PBASE>
double
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::yPIDImpANN::getOutput( const LHCb::ProtoParticle* proto ) const {
  // Fill the array of network inputs
  const auto& ins = this->inputs();
  std::transform( ins.begin(), ins.end(), m_vars.begin(),
                  [&proto]( const auto& input ) { return input->value( proto ); } );

  // FPE Guard for MVA call
  FPE::Guard guard( true );

  // get the output response and return
  return m_reader->GetMvaValue( m_vars );
}

//=============================================================================
// NetConfig Constructor
//=============================================================================
template <class PBASE>
ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::NetConfig::NetConfig(
    const std::string& tkType, const std::string& pidType, const std::string& netVersion,
    const bool suppressANNPrintout, const ChargedProtoANNPIDCommonBase<PBASE>* parent ) {
  // Assume init will go OK until proved otherwise
  bool OK = true;

  // Check the configuration
  if ( tkType.empty() ) {
    parent->Error( "No TrackType specified" ).ignore();
    OK = false;
  }
  if ( pidType.empty() ) {
    parent->Error( "No PIDType specified" ).ignore();
    OK = false;
  }
  if ( netVersion.empty() ) {
    parent->Error( "No NetVersion specified" ).ignore();
    OK = false;
  }

  // Yandex PID ? (Really is not ideal that this is required...)
  const bool isYandex =
      ( netVersion.find( "DNN" ) != std::string::npos || netVersion.find( "FLAT4d" ) != std::string::npos ||
        netVersion.find( "CatBoost" ) != std::string::npos || netVersion.find( "XGBoost" ) != std::string::npos );
  // If yandex and not Long, use placeholder MVA
  // Yandex PID only supports Long tracks :((
  if ( isYandex && "Long" != tkType ) {
    parent->warning() << "Yandex MVA " << netVersion << " does not support " << tkType << " tracks" << endmsg;
    m_netHelper = std::make_unique<NullANN>();
  } else {

    // Config file name
    const std::string filename_extension = "_ANN.txt";
    const std::string configFile         = "GlobalPID_" + pidType + "_" + tkType + filename_extension;

    // Determine where to load the data files from.
    // First try via env var.
    const std::string paramEnv = "CHARGEDPROTOANNPIDROOT";
    if ( !getenv( paramEnv.c_str() ) ) {
      OK = false;
      parent->Error( "$" + paramEnv + " not set" ).ignore();
    }
    std::string   paramRoot = ( std::string( getenv( paramEnv.c_str() ) ) + "/data/" + netVersion + "/" );
    std::ifstream config( ( paramRoot + configFile ).c_str() );
    if ( !config.is_open() ) {
      // That failed. So try as a local file (i.e. in Ganga).
      parent->Warning( "Failed to open " + paramRoot + configFile + ".. Trying local file..." ).ignore();
      paramRoot = "data/" + netVersion + "/";
      config.open( ( paramRoot + configFile ).c_str() );
    }

    // Open the config file
    if ( config.is_open() ) {
      // Read the particle type
      config >> m_particleType;
      if ( !boost::iequals( m_particleType, pidType ) ) {
        OK = false;
        parent->Error( "Mis-match in particle types " + m_particleType + " != " + pidType ).ignore();
      }

      // Read the track Type
      config >> m_trackType;
      if ( !boost::iequals( m_trackType, tkType ) ) {
        OK = false;
        parent->Error( "Mis-match in track types " + m_trackType + " != " + tkType ).ignore();
      }

      // Track selection cuts file name
      std::string cutsFile;
      config >> cutsFile;

      // Check if this string ends with .txt ?
      if ( cutsFile.find( ".txt" ) != std::string::npos ) {
        // New style cuts file(s), so read from file(s)

        // Split up files seperated by comma
        std::vector<std::string> files;
        boost::split( files, cutsFile, boost::is_any_of( "," ) );
        for ( const auto& file : files ) {

          // open cuts file
          const auto    fullFile = paramRoot + file;
          std::ifstream cuts( fullFile.c_str() );

          // If OK, read
          if ( UNLIKELY( !cuts.is_open() ) ) {
            OK = false;
            parent->Error( "Track Selection cuts file '" + fullFile + "' cannot be opened" ).ignore();
          } else {

            // Read the cuts and create a cut object for each
            std::string cut;
            while ( std::getline( cuts, cut ) ) {
              // Skip empty lines or comments
              if ( !cut.empty() && cut.find( "#" ) == std::string::npos ) {
                // try and make a cut for this string
                m_cuts.emplace_back( new Cut( cut, parent ) );
                if ( !m_cuts.back()->isOK() ) {
                  OK = false;
                  parent->Error( "Failed to decode selection cut '" + cut + "'" ).ignore();
                }
              }
            }

            // close the file
            cuts.close();
          }

        } // loop over cuts file

      } else {

        // Old style, so last line and the next few are cut values themselves
        std::string minP, minPt, maxChiSq, minLikeli, maxGhostProb, trackPreSel;
        minP = cutsFile;
        config >> minPt;
        config >> maxChiSq;
        config >> minLikeli;
        config >> maxGhostProb;
        config >> trackPreSel;

        // Create cut objects
        bool _ok = true;
        m_cuts.emplace_back( new Cut( "TrackP                > " + minP, parent ) );
        _ok &= m_cuts.back()->isOK();
        m_cuts.emplace_back( new Cut( "TrackPt               > " + minPt, parent ) );
        _ok &= m_cuts.back()->isOK();
        m_cuts.emplace_back( new Cut( "TrackChi2PerDof       < " + maxChiSq, parent ) );
        _ok &= m_cuts.back()->isOK();
        m_cuts.emplace_back( new Cut( "TrackLikelihood       > " + minLikeli, parent ) );
        _ok &= m_cuts.back()->isOK();
        m_cuts.emplace_back( new Cut( "TrackGhostProbability < " + maxGhostProb, parent ) );
        _ok &= m_cuts.back()->isOK();
        if ( trackPreSel == "TrackPreSelIsMuon" ) {
          m_cuts.emplace_back( new Cut( "MuonIsMuon > 0.5", parent ) );
          _ok &= m_cuts.back()->isOK();
        }
        if ( !_ok ) {
          OK = false;
          parent->Error( "Failed to decode old style track cuts" ).ignore();
        }
      }

      // Read the network type
      std::string annType;
      config >> annType;

      // read parameters file name
      std::string paramFileName;
      config >> paramFileName;
      paramFileName = paramRoot + paramFileName; // add full path

      // test opening of network parameters file
      {
        std::ifstream netparamtest( paramFileName.c_str() );
        if ( !netparamtest.is_open() ) {
          parent->Error( "Network parameters file '" + paramFileName + "' cannot be opened" ).ignore();
          OK = false;
        }
        netparamtest.close();
      }

      // Read the list of inputs
      std::string  input;
      StringInputs inputs;
      while ( config >> input ) {
        // Skip empty lines and comments
        if ( !input.empty() && input.find( "#" ) == std::string::npos ) { inputs.emplace_back( input ); }
      }
      // if all is OK so far, Load the network helper object
      if ( OK ) {
        if ( "TMVA" == annType ) {
          // First see if we have a built in C++ implementation for this case
          m_netHelper = std::make_unique<TMVAImpANN>( netVersion, particleType(), trackType(), inputs, parent );
          if ( !m_netHelper->isOK() ) {
            // No, so try again with a TMVA Reader
            parent->warning() << "Compiled TMVA implementation not available for " << netVersion << " "
                              << particleType() << " " << trackType() << " -> Reverting to Generic XML Reader"
                              << endmsg;
            m_netHelper = std::make_unique<TMVAReaderANN>( paramFileName, inputs, parent, suppressANNPrintout );
          }
        }
#ifdef _ENABLE_NEUROBAYES
        else if ( "NeuroBayes" == annType ) {
          if ( parent->msgLevel( MSG::DEBUG ) ) parent->debug() << "Using NeuroBayes Expert implementation" << endmsg;
          // FPE Guard for NB call
          FPE::Guard guard( true );
          m_netHelper = std::make_unique<NeuroBayesANN>( paramFileName, inputs, parent, suppressANNPrintout );
        }
#endif
        else if ( isYandex ) {
          if ( parent->msgLevel( MSG::DEBUG ) ) {
            parent->debug() << "Using yPID implementation" << netVersion << " " << particleType() << " " << trackType()
                            << endmsg;
          }
          m_netHelper = std::make_unique<yPIDImpANN>( netVersion, particleType(), trackType(), inputs, parent );
        } else {
          parent->Error( "Unknown ANN type '" + annType + "'" ).ignore();
          OK = false;
        }

        // is the final helper OK ?
        if ( OK && m_netHelper.get() && m_netHelper->isOK() ) {
          // print a summary of the configuration
          const std::string sF = "ANNPID : Tune=%-13s TrackType=%-12s Particle=%-12s";
          parent->info() << boost::format( sF ) % netVersion % trackType() % particleType() << endmsg;
          if ( parent->msgLevel( MSG::DEBUG ) ) {
            parent->debug() << "Classifier type     = " << annType << endmsg << "ConfigFile       = " << paramRoot
                            << configFile << endmsg << "ParamFile        = " << paramFileName << endmsg
                            << "ANN inputs (" << inputs.size() << ")  = " << inputs << endmsg << "Preselection Cuts ("
                            << m_cuts.size() << ") = " << m_cuts << endmsg;
          }
        } else {
          parent->Error( "Problem configuring classifier" ).ignore();
          OK = false;
        }
      }

    } else {
      OK = false;
      parent->Error( "Failed to open configuration file '" + paramRoot + configFile + "'" ).ignore();
    }

    // Close the config file
    config.close();

    // If something went wrong, clean up
    if ( !OK ) { cleanUp(); }

  } // is not yandex
}

template <class PBASE>
void ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::NetConfig::cleanUp() {
  m_netHelper.reset( nullptr );
  m_cuts.clear();
}

template <class PBASE>
bool ANNGlobalPID::ChargedProtoANNPIDCommonBase<PBASE>::NetConfig::passCuts( const LHCb::ProtoParticle* proto ) const {
  return std::all_of( m_cuts.begin(), m_cuts.end(), [&proto]( const auto& cut ) { return cut->isSatisfied( proto ); } );
}
