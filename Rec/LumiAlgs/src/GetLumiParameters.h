/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GETLUMIPARAMETERS_H
#define GETLUMIPARAMETERS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IGetLumiParameters.h" // Interface

// CondDB
#include "DetDesc/Condition.h"
#include "GaudiKernel/IDetDataSvc.h"

// TCK
#include "GaudiKernel/SmartDataPtr.h"
#include "Kernel/IPropertyConfigSvc.h"
#include "Kernel/IRateFromTCK.h"
#include "Kernel/TCK.h"

/** @class GetLumiParameters GetLumiParameters.h
 *
 *  Tool that returns all info needed for luminosity calculation
 *
 *  @author Jaap Panman
 *  @date   2009-03-16
 */
class GetLumiParameters : public extends<GaudiTool, IGetLumiParameters> {
public:
  /// Standard constructor
  using base_class::base_class;

  StatusCode initialize() override; ///< Init DB access

  long   CollidingBunches() override; ///< retrieve number of colliding bunches
  double RandomRateBB() override;     ///< retrieve random bunch-bunch rate in HLT
  /// retrieve TCK
  inline unsigned int getTCK() const override { return m_triggerTCK; };
  /// retrieve Odin Random BB fraction
  inline double OdinFraction() const override { return m_odinFraction; };
  /// retrieve random rate in HLT
  inline double HLTRandomRate() const override { return m_rateHLT; };
  /// retrieve revolution frequency
  inline double LHCFrequency() const override { return m_calibRevolutionFrequency; };
  /// relative calibration factors
  inline std::vector<double> CalibRelative() const override { return m_calibRelative; };
  /// usage factors
  inline std::vector<double> CalibCoefficients() const override { return m_calibCoefficients; };
  /// relative calibration factors
  inline std::vector<double> CalibRelativeLog() const override { return m_calibRelativeLog; };
  /// usage factors
  inline std::vector<double> CalibCoefficientsLog() const override { return m_calibCoefficientsLog; };
  /// absolute scale
  inline double CalibScale() const override { return m_calibScale; };
  /// absolute scale error
  inline double CalibScaleError() const override { return m_calibScaleError; };
  /// status scale flag
  inline double StatusScale() const override { return m_statusScale; };

protected:
  virtual StatusCode   registerDB(); ///< register DB conditions
  virtual StatusCode   processDB();  ///< DB checking code
  SmartIF<IDetDataSvc> m_dds;        ///< DetectorDataSvc

private:
  std::string m_ToolName; ///< name of tool

  StatusCode i_cacheRelativeData();       ///< Function extracting data from Condition
  StatusCode i_cacheRelativeDataLog();    ///< Function extracting data from Condition
  StatusCode i_cacheAbsoluteData();       ///< Function extracting data from Condition
  StatusCode i_cacheCoefficientData();    ///< Function extracting data from Condition
  StatusCode i_cacheCoefficientDataLog(); ///< Function extracting data from Condition
  StatusCode i_cacheSamplingData();       ///< Function extracting data from Condition
  StatusCode i_cacheTriggerData();        ///< Function extracting data from Condition
  StatusCode i_cacheFillingData();        ///< Function extracting data from Condition

  // database conditions and calibration factors
  Condition* m_condRelative        = nullptr; ///< Condition for relative calibration
  Condition* m_condRelativeLog     = nullptr; ///< Condition for relative calibration
  Condition* m_condAbsolute        = nullptr; ///< Condition for absolute scale
  Condition* m_condCoefficients    = nullptr; ///< Condition for usage coefficients
  Condition* m_condCoefficientsLog = nullptr; ///< Condition for usage coefficients
  Condition* m_condSampling        = nullptr; ///< Condition for sampling coefficients
  Condition* m_condTrigger         = nullptr; ///< Condition for sampling coefficients
  Condition* m_condFilling         = nullptr; ///< Condition for LHC filling scheme

  std::vector<double> m_calibRelative;            ///< relative calibration factors
  std::vector<double> m_calibCoefficients;        ///< usage factors
  std::vector<double> m_calibRelativeLog;         ///< relative calibration factors
  std::vector<double> m_calibCoefficientsLog;     ///< usage factors
  double              m_statusScale;              ///< absolute scale  set to zero if no lumi
  double              m_calibScale;               ///< absolute scale
  double              m_calibScaleError;          ///< absolute scale error
  double              m_calibRevolutionFrequency; ///< revolution frequency (Hz)
  double              m_calibRandomFrequencyBB;   ///< random lumi event frequency of BB crossings (Hz)
  long                m_calibCollidingBunches;    ///< number of colliding bunches
  unsigned int        m_triggerTCK;               ///< tck for these data
  unsigned int        m_knownTCK;                 ///< previous value
  std::vector<double> m_lumiPars;                 ///< trigger LumiPars
  double              m_odinTotalRate;            ///< total ODIN random rate
  double              m_odinFraction;             ///< fraction of total spent during BB
  double              m_rateHLT;                  ///< random lumi rate set by HLT line
  double              m_rateBB;                   ///< random lumi rate set by HLT line for BB
  long                m_B1NBunches;               ///< filling scheme data
  long                m_B2NBunches;               ///< filling scheme data
  long                m_NCollidingBunches;        ///< filling scheme data
  long                m_B1WrongBucketFlag;        ///< filling scheme data
  long                m_B2WrongBucketFlag;        ///< filling scheme data
  long                m_onlineCollidingBunches;   ///< number of colliding bunches

  Gaudi::Property<std::string> m_instanceName{this, "InstanceName", "Hlt1LumiODINFilter"};
  Gaudi::Property<bool>        m_useOnline{this, "UseOnline", true}; ///< flag to use online partition of DB
  bool                         m_doneInit  = false;                  ///< flag to indicate intitalization
  IRateFromTCK*                m_tckReader = nullptr;                ///< Property Config Service
};
#endif // GETLUMIPARAMETERS_H
