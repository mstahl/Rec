/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/STLExtensions.h"
#include <utility>

template <typename ValueType>
class Chunker {
public:
  using index_type = typename LHCb::span<ValueType>::index_type;

private:
  const LHCb::span<ValueType> m_data;
  const index_type            m_chunkSize;
  struct Sentinel {};
  class Iterator {
    LHCb::span<ValueType> m_remainder;
    const index_type      m_chunkSize;

  public:
    Iterator( LHCb::span<ValueType> data, index_type chunkSize )
        : m_remainder{std::move( data )}, m_chunkSize{chunkSize} {
      assert( m_chunkSize > 0 );
    }
    LHCb::span<ValueType> operator*() const { return m_remainder.first( std::min( m_remainder.size(), m_chunkSize ) ); }
    bool                  operator!=( Sentinel ) const { return !m_remainder.empty(); }
    Iterator&             operator++() {
      m_remainder = m_remainder.subspan( std::min( m_remainder.size(), m_chunkSize ) );
      return *this;
    }
  };

public:
  Chunker( LHCb::span<ValueType> in, index_type chunkSize ) : m_data{std::move( in )}, m_chunkSize{chunkSize} {}
  Iterator begin() const { return {m_data, m_chunkSize}; };
  Sentinel end() const { return {}; }
};

template <typename Container, typename Int>
Chunker( const Container&, Int )->Chunker<std::add_const_t<typename Container::value_type>>;
template <typename Container, typename Int>
Chunker( Container&, Int )->Chunker<typename Container::value_type>;
