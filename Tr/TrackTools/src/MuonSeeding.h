/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONSEEDING_H
#define MUONSEEDING_H 1

// Include files
// from Gaudi
#include "Event/Particle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "MuonInterfaces/IMuonTrackMomRec.h"
#include "MuonInterfaces/IMuonTrackRec.h"
#include "MuonInterfaces/MuonHit.h"
#include "MuonInterfaces/MuonTrack.h"

#include "Event/RecVertex.h"
#include "Event/State.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"

#include "GaudiKernel/Chrono.h"
#include "Kernel/LHCbID.h"

/** @class MuonSeeding MuonSeeding.h
 *
 * \brief  Make a MuonSeeding: Get muon standalone tracks
 *
 * Parameters:
 * - ToolName: Name for the tool that makes muon standalone track.
 * - Extrapolator: Name for the track extrapolator.
 * - FillMuonStubInfo: Fill parameters of muon stub in info fields of track;
 * - Output: The location the tracks should be written to.
 *
 *  @author Michel De Cian
 *  @date   2010-09-20
 */

class MuonSeeding final : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override; ///< Algorithm execution

private:
  // -- Methods
  StatusCode fillPVs( std::vector<double>& PVPos );
  StatusCode iterateToPV( LHCb::Track* track, LHCb::State& muonState, LHCb::State& veloState,
                          const std::vector<double>& PVPos, double qOverP );

  // -- Properties
  Gaudi::Property<bool>        m_fitTracks{this, "FitTracks", true};
  Gaudi::Property<std::string> m_outputLoc{this, "Output", "Rec/MuonSeeding/Tracks"};

  // -- Tools
  ToolHandle<IMuonTrackRec>      m_trackTool{this, "MuonRecTool", "MuonNNetRec/MuonRecTool"};
  ToolHandle<ITrackFitter>       m_trackFitter{this, "Fitter", "TrackMasterFitter/Fitter"};
  ToolHandle<IMuonTrackMomRec>   m_momentumTool{this, "MuomMomTool", "MuonTrackMomRec/MuonMomTool"};
  ToolHandle<ITrackExtrapolator> m_extrapolator{this, "Extrapolator", "TrackMasterExtrapolator/Extrapolator"};
};
#endif // GETMUONTRACK_H
