/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKMONITOR_H
#define TRACKMONITOR_H 1

#include "TrackMonitorBase.h"

#include "Event/Track.h"
#include "TrackInterfaces/IHitExpectation.h"

namespace LHCb {
  class State;
}

/** @class TrackMonitor TrackMonitor.h "TrackCheckers/TrackMonitor"
 *
 * Class for track monitoring
 *  @author M. Needham.
 *  @date   6-5-2007
 */

class TrackMonitor : public TrackMonitorBase {

public:
  /** Standard construtor */
  using TrackMonitorBase::TrackMonitorBase;

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  void fillHistograms( const LHCb::Track& track, const std::string& type ) const;

  void findRefStates( const LHCb::Track& track, const LHCb::State*& firstMeasurementState,
                      const LHCb::State*& lastMeasurementState ) const;

  Gaudi::Property<double> m_maxMomentum{this, "MaxMomentum", 100.};
  Gaudi::Property<double> m_maxChi2Dof{this, "MaxChi2Dof", 5.};

  ToolHandle<IHitExpectation> m_utExpectation{this, "UTExpectation", "UTHitExpectation"};

  std::map<std::string, unsigned int> m_multiplicityMap;
};

#endif // TRACKMONITOR_H
