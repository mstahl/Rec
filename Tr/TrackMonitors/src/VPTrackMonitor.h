/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/Vector3DTypes.h"

// LHCb
// Det/VPDet
#include "VPDet/DeVP.h"

// Rec
// Tr/TrackFitEvent
#include "Event/FitNode.h"

/** @class VPTrackMonitor VPTrackMonitor.h
 *
 *
 *  @author Christoph Hombach
 *  @date   2015-01-08
 */
class VPTrackMonitor : public GaudiTupleAlg {
public:
  /// Standard constructor
  VPTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  // TES location of tracks
  std::string m_trackLocation;
  std::string m_clusterLocation;
  std::string m_linkedHitsLocation;

  /// Detector element
  DeVP* m_det = nullptr;

  Gaudi::XYZVector getResidual( const Gaudi::XYZPoint& cluster, const DeVPSensor& sensor,
                                const LHCb::FitNode& fitNode ) const;
};
