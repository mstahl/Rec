/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MOVERECVERTEXTRACKS_H
#define MOVERECVERTEXTRACKS_H 1

/** @class MoveRecVertexTracks MoveRecVertexTracks.h
 *
 *  Copy tracks participating in RecVertices to a new container and
 *  update the vertex' track pointers.
 *
 *  @author Rosen Matev
 *  @date   2016-04-16
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

class MoveRecVertexTracks : public GaudiAlgorithm {
public:
  /// Standard constructor
  MoveRecVertexTracks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  std::string m_vertexLocation;
  std::string m_outputLocation;
};

#endif // MOVERECVERTEXTRACKS_H
