/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackCloneCleaner_H_
#define _TrackCloneCleaner_H_

#include "GaudiAlg/GaudiAlgorithm.h"
/** @class TrackCloneCleaner TrackCloneCleaner.h
 *
 *  Clean out clone tracks, using information from the Clone linker table
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */
class TrackCloneCleaner final : public GaudiAlgorithm {

public:
  // Constructors and destructor
  TrackCloneCleaner( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode execute() override;

private:
  std::string              m_inputLocation;  ///< Locations of Tracks in TES
  std::vector<std::string> m_inputLocations; ///< Locations of Tracks in TES
  std::string              m_linkerLocation; ///< Location of Clone linker in TES
  double                   m_cloneCut;       ///< Clone cut value
};

#endif
