###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackCheckers
################################################################################
gaudi_subdir(TrackCheckers v4r23)

gaudi_depends_on_subdirs(Event/LinkerEvent
                         Event/MCEvent
                         Event/RecEvent
                         Event/TrackEvent
                         GaudiAlg
                         GaudiUtils
                         Kernel/MCInterfaces
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces
                         Tr/TrackKernel)

find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TrackCheckers
                 src/*.cpp
                 INCLUDE_DIRS GSL AIDA Kernel/MCInterfaces Tr/TrackInterfaces
                 LINK_LIBRARIES GSL LinkerEvent MCEvent RecEvent TrackEvent GaudiAlgLib GaudiUtilsLib TrackFitEvent TrackKernel)

