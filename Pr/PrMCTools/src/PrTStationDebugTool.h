/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRTSTATIONDEBUGTOOL_H
#define PRTSTATIONDEBUGTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "PrKernel/IPrDebugTool.h" // Interface

/** @class PrTStationDebugTool PrTStationDebugTool.h
 *
 *
 *  @author Olivier Callot
 *  @date   2012-03-22
 */
class PrTStationDebugTool : public GaudiTool, virtual public IPrDebugTool {
public:
  /// Standard constructor
  PrTStationDebugTool( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~PrTStationDebugTool(); ///< Destructor

  bool matchKey( LHCb::LHCbID id, int key ) const override;

  void printKey( MsgStream& msg, LHCb::LHCbID id ) const override;

protected:
private:
};
#endif // PRTSTATIONDEBUGTOOL_H
