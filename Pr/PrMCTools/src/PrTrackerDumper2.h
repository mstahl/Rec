/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef PRTRACKERDUMPER2_H
#define PRTRACKERDUMPER2_H 1

// Include files
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/ODIN.h"
#include "Event/Track.h"
#include "Event/VPLightCluster.h"
#include "GaudiAlg/Consumer.h"
#include "Linker/LinkerTool.h"
#include "Linker/LinkerWithKey.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"

// ROOT
#include <TFile.h>
#include <TTree.h>

#include <mutex>

/** @class PrTrackerDumper PrTrackerDumper2.h
 *  Algorithm to store tracking hits to a single root file
 *  (PrTrackerDumper stores events in separate files)
 *
 *  @author Renato Quagliani
 *  @date   2017-11-06
 */
/*

*/

class PrTrackerDumper2 : public Gaudi::Functional::Consumer<void(
                             const LHCb::MCParticles&, const LHCb::VPLightClusters&, const PrFTHitHandler<PrHit>&,
                             const UT::HitHandler&, const LHCb::ODIN&, const LHCb::LinksByKey& )> {
public:
  /// Standard constructor
  PrTrackerDumper2( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode finalize() override;

  void operator()( const LHCb::MCParticles& MCParticles, const LHCb::VPLightClusters& VPClusters,
                   const PrFTHitHandler<PrHit>& ftHits, const UT::HitHandler& utHits, const LHCb::ODIN& odin,
                   const LHCb::LinksByKey& links ) const override;

private:
  std::unique_ptr<TFile> file;
  std::unique_ptr<TTree> tree;

  mutable std::mutex mutex;
  mutable int        eventID = 0;

  mutable bool   fullInfo      = false;
  mutable bool   hasSciFi      = false;
  mutable bool   hasUT         = false;
  mutable bool   hasVelo       = false;
  mutable bool   isDown        = false;
  mutable bool   isDown_noVelo = false;
  mutable bool   isLong        = false;
  mutable bool   isLong_andUT  = false;
  mutable double p             = 0;
  mutable double px = 0, py = 0, pz = 0;
  mutable double pt  = 0;
  mutable double eta = 0, phi = 0;
  mutable double eta_track = 0, phi_track = 0, chi2_track = 0;
  // vertex origin of the particle
  mutable double                    ovtx_x                = 0;
  mutable double                    ovtx_y                = 0;
  mutable double                    ovtx_z                = 0;
  mutable int                       pid                   = 0;
  mutable bool                      fromBeautyDecay       = false;
  mutable bool                      fromCharmDecay        = false;
  mutable bool                      fromStrangeDecay      = false;
  mutable int                       DecayOriginMother_pid = 0;
  mutable int                       key                   = 0;
  mutable int                       nVeloHits_track       = 0;
  mutable std::vector<float>        Velo_x_track;
  mutable std::vector<float>        Velo_y_track;
  mutable std::vector<float>        Velo_z_track;
  mutable std::vector<int>          Velo_Module_track;
  mutable std::vector<int>          Velo_Sensor_track;
  mutable std::vector<int>          Velo_Station_track;
  mutable std::vector<unsigned int> Velo_lhcbID_track;
  mutable std::vector<unsigned int> Velo_index_track;

  mutable int                       nVeloHits = 0;
  mutable std::vector<float>        Velo_x;
  mutable std::vector<float>        Velo_y;
  mutable std::vector<float>        Velo_z;
  mutable std::vector<int>          Velo_Module;
  mutable std::vector<int>          Velo_Sensor;
  mutable std::vector<int>          Velo_Station;
  mutable std::vector<unsigned int> Velo_lhcbID;
  mutable std::vector<unsigned int> Velo_index;

  mutable std::vector<float>        FT_hitx;
  mutable std::vector<float>        FT_hitz;
  mutable std::vector<float>        FT_hitw;
  mutable std::vector<float>        FT_hitDXDY;
  mutable std::vector<float>        FT_hitYMin;
  mutable std::vector<float>        FT_hitYMax;
  mutable std::vector<int>          FT_hitPlaneCode;
  mutable std::vector<int>          FT_hitzone;
  mutable std::vector<unsigned int> FT_lhcbID;

  mutable int nFTHits         = 0;
  mutable int nbHits_in_UT    = 0;
  mutable int nbHits_in_SciFi = 0;

  mutable std::vector<float>        UT_cos;
  mutable std::vector<float>        UT_cosT;
  mutable std::vector<float>        UT_dxDy;
  mutable std::vector<unsigned int> UT_lhcbID;
  mutable std::vector<int>          UT_planeCode;
  mutable std::vector<float>        UT_sinT;
  mutable std::vector<int>          UT_size;
  mutable std::vector<float>        UT_tanT;
  mutable std::vector<float>        UT_weight;
  mutable std::vector<float>        UT_xAtYEq0;
  mutable std::vector<float>        UT_xAtYMid;
  mutable std::vector<float>        UT_xMax;
  mutable std::vector<float>        UT_xMin;
  mutable std::vector<float>        UT_xT;
  mutable std::vector<float>        UT_yBegin;
  mutable std::vector<float>        UT_yEnd;
  mutable std::vector<float>        UT_yMax;
  mutable std::vector<float>        UT_yMid;
  mutable std::vector<float>        UT_yMin;
  mutable std::vector<float>        UT_zAtYEq0;

  mutable int nUTHits = 0;

  Gaudi::Property<std::string> m_track_location{
      this, "TrackLocation", "Rec/Track/Keyed/Velo",
      "Cointainer in which are stored reconstructed traks linked to MC particles"};
};
#endif // PRTRACKERDUMPER2_H
