/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/Filter.h"
#include "Functors/with_functors.h"
#include "GaudiAlg/Transformer.h"

#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/PrIterableVeloTracks.h"
#include "Event/PrZip.h"
#include "Event/TrackWithMuonPIDSkin.h"
#include "Event/Track_v2.h"
#include "PrKernel/PrSelection.h"
#include "SOAContainer/SOAContainer.h"
#include "SOAExtensions/ZipSelection.h"
#include "SelKernel/TrackZips.h"

#include <vector>

namespace {
  template <typename T>
  struct TypeHelper {};

  template <>
  struct TypeHelper<Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>> {
    using T = Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>;
    using AlgInputType                 = T const&;
    using InputType                    = Zipping::SelectionView<T const> const&;
    using OutputType                   = Zipping::ExportedSelection<>;
    constexpr static auto ExtraHeaders = {"Event/MuonPID_v2.h", "Event/TrackWithMuonPIDSkin.h",
                                          "SOAExtensions/ZipSelection.h", "SOAContainer/SOAContainer.h"};
  };

  /** Helper for compile time check if a type is a filtered-zip type
   */
  template <typename>
  struct must_be_wrapped : std::false_type {};

  // TODO don't want to write detail here...
  template <typename... T>
  struct must_be_wrapped<LHCb::Pr::detail::merged_t<T...>> : std::true_type {};

  template <typename T>
  inline constexpr bool must_be_wrapped_v = must_be_wrapped<T>::value;

  // Given the tag type T, TypeHelper tells us both the return type of the
  // functor (OutputType) and whether this return type is to be converted into
  // a non-owning view (WrapOutput), which means that we can deduce the output
  // type of the convert() function too, just using the tag type T. This is the
  // return type of operator() in the algorithm. We also need to deduce that
  // type with the leading bool stripped off, so that we can write down the
  // name of the functional framework base class that the algorithm inherits
  // from
  template <typename TagType>
  struct OutputHelper {
    using FilteredType               = Functors::filtered_t<TagType>;
    static constexpr bool WrapOutput = must_be_wrapped_v<FilteredType>;

    template <typename KeyValue>
    static auto names() {
      if constexpr ( WrapOutput ) {
        return std::array<KeyValue, 2>{{KeyValue{"Output", ""}, KeyValue{"OutputStorage", ""}}};
      } else {
        return KeyValue{"Output", ""};
      }
    }

    static auto convert( bool filter_passed, FilteredType&& filtered ) {
      if constexpr ( WrapOutput ) {
        // OutputType is some owning type that we don't want to use directly, but
        // which we need to keep alive so that we can use a non-owning view into
        // it. Achieve this by wrapping it up in a unique_ptr -- so the address
        // of the contained object is stable -- and returning that.
        auto storage = std::make_unique<FilteredType>( std::move( filtered ) );
        // Make a non-owning view, which will store the address of the object
        // hidden inside the unique_ptr.
        auto view = LHCb::Pr::make_zip( *storage.get() );
        return std::tuple{filter_passed, std::move( view ), std::move( storage )};
      } else {
        return std::tuple{filter_passed, std::move( filtered )};
      }
    }

    // this is std::tuple<bool, A[, B]>
    using AlgorithmOutput = std::invoke_result_t<decltype( convert ), bool, FilteredType>;

    // helper to strip off the bool from the type
    template <typename T>
    struct remove_first_type {};

    template <typename T, typename... Ts>
    struct remove_first_type<std::tuple<T, Ts...>> {
      using type = std::tuple<Ts...>;
    };

    // this is std::tuple<A[, B]>
    using DataTuple = typename remove_first_type<AlgorithmOutput>::type;
  };

  // Just shorthand for below
  template <typename T>
  using FilterTransform = Gaudi::Functional::MultiTransformerFilter<typename OutputHelper<T>::DataTuple( T const& )>;

  template <typename T>
  using SOAFilterTransform = Gaudi::Functional::MultiTransformerFilter<std::tuple<typename TypeHelper<T>::OutputType>(
      typename TypeHelper<T>::AlgInputType, Zipping::ExportedSelection<> const& )>;

  template <typename T>
  struct FilterCut {
    constexpr static auto PropertyName = "Cut";
    constexpr static auto ExtraHeaders = LHCb::header_map_v<T>;
    using Signature                    = Functors::filtered_t<T>( T const& );
  };

  template <typename T>
  struct SOAFilterCut {
    constexpr static auto PropertyName = "Cut";
    constexpr static auto ExtraHeaders = TypeHelper<T>::ExtraHeaders;
    using InputType                    = typename TypeHelper<T>::InputType;
    using Signature                    = Functors::filtered_t<InputType>( InputType );
  };
} // namespace

namespace Pr {
  /** @class Filter PrFilter.cpp
   *
   *  Filter<T> applies a selection to an input Selection<T> and returns a new Selection<T> object.
   *
   *  @tparam T The selected object type (e.g. Track, Particle, ...). By contruction this is not copied, as the
   *            input/output type Selection<T> is just a view of some other underlying storage.
   */
  template <typename T>
  class Filter final : public with_functors<FilterTransform<T>, FilterCut<T>> {
  public:
    using Base    = with_functors<FilterTransform<T>, FilterCut<T>>;
    using OHelper = OutputHelper<T>;

    Filter( const std::string& name, ISvcLocator* pSvcLocator )
        : Base( name, pSvcLocator, {"Input", ""}, OHelper::template names<typename Base::KeyValue>() ) {}

    // Return type is std::tuple<bool, A[, B]>
    typename OHelper::AlgorithmOutput operator()( T const& in ) const override {
      // Get the functor from the with_functors mixin
      auto const& pred = this->template getFunctor<FilterCut<T>>();

      // Apply the functor to get something we can return
      auto filtered = pred( in );

      // Update the statistics. Maybe the interface has changed and we can do this more elegantly?
      auto buffer = m_cutEff.buffer();
      // Avoid sign-comparison and promotion warnings
      using size_t = std::common_type_t<decltype( in.size() ), decltype( filtered.size() )>;
      for ( size_t i = 0; i < size_t( in.size() ); ++i ) { buffer += i < size_t( filtered.size() ); }

      // For use in the control flow: did we select anything?
      auto filter_pass = !filtered.empty();

      // Add an extra conversion step if requested
      return OHelper::convert( filter_pass, std::move( filtered ) );
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};
  };

  template <typename T>
  class SOAFilter final : public with_functors<SOAFilterTransform<T>, SOAFilterCut<T>> {
  public:
    using Base       = with_functors<SOAFilterTransform<T>, SOAFilterCut<T>>;
    using InputType  = typename TypeHelper<T>::AlgInputType;
    using OutputType = typename TypeHelper<T>::OutputType;
    using KeyValue   = typename SOAFilterTransform<T>::KeyValue;

    SOAFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : Base( name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"InputSelection", ""}}, KeyValue{"Output", ""} ) {}

    std::tuple<bool, OutputType> operator()( InputType in, Zipping::ExportedSelection<> const& in_sel ) const override {
      // Apply the input selection to our input data
      auto selected_input = Zipping::SelectionView{&in, in_sel};

      // Get the functor from the with_functors mixin
      auto const& pred = this->template getFunctor<SOAFilterCut<T>>();

      // Apply the functor to get the refined selection
      auto filtered = pred( selected_input );

      // Update the statistics. Maybe the interface has changed and we can do this more elegantly?
      auto buffer = m_cutEff.buffer();
      for ( auto i = 0ul; i < selected_input.size(); ++i ) { buffer += i < filtered.size(); }

      // For use in the control flow: did we select anything?
      auto filter_pass = !filtered.empty();

      return {filter_pass, std::move( filtered )};
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};
  };

  // Pr::Selection<v2::Track> -> Pr::Selection<v2::Track>
  DECLARE_COMPONENT_WITH_ID( Filter<Pr::Selection<LHCb::Event::v2::Track>>, "PrFilter__Track_v2" )

  // LHCb::Pr::Velo::Tracks -> LHCb::Pr::Velo::Tracks
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Velo::Tracks>, "PrFilter__PrVeloTracks" )

  // LHCb::Pr::Fitted::Forward::Tracks -> LHCb::Pr::Fitted::Forward::Tracks
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::Tracks>, "PrFilter__PrFittedForwardTracks" )

  // LHCb::Pr::Fitted::Forward::TracksWithPVs
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::TracksWithPVs>,
                             "PrFilter__PrFittedForwardTracksWithPVs" )

  // LHCb::Pr::Fitted::Forward::TracksWithMuonID
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                             "PrFilter__PrFittedForwardTracksWithMuonID" )

  // SOA::Container: refine a selection on a view
  DECLARE_COMPONENT_WITH_ID(
      SOAFilter<Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>>,
      "SOAFilter__TrackWithMuonIDView" )
} // namespace Pr
