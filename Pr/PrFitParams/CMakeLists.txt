###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PrFitParams
################################################################################
gaudi_subdir(PrFitParams v2r0p1)

gaudi_depends_on_subdirs(Event/MCEvent
                         Event/LinkerEvent
                         Event/TrackEvent
                         GaudiAlg
                         GaudiKernel)

find_package(AIDA)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(PrFitParamsLib
                 src/*.cpp
                 PUBLIC_HEADERS PrFitParams
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES GaudiAlgLib GaudiKernel MCEvent LinkerEvent TrackEvent)

gaudi_add_module(PrFitParams
                 src/*.cpp
                 INCLUDE_DIRS AIDA PrFitParams
                 LINK_LIBRARIES PrFitParamsLib GaudiAlgLib GaudiKernel MCEvent LinkerEvent TrackEvent)
