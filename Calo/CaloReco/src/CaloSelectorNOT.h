/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALORECO_CALOSELECTORNOT_H
#define CALORECO_CALOSELECTORNOT_H 1
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiTool.h"
// From CaloInterfaces
#include "CaloInterfaces/ICaloClusterSelector.h"
/** @class CaloSelectorNOT CaloSelectorNOT.h
 *
 *  Helper concrete tool for selection of calocluster objects
 *  This selector selects the cluster if
 *  none  of its daughter selector select it!
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   27/04/2002
 */
class CaloSelectorNOT : public virtual ICaloClusterSelector, public GaudiTool {
public:
  using Names      = std::vector<std::string>;
  using Selectors  = std::vector<ICaloClusterSelector*>;
  using IncCounter = Gaudi::Accumulators::Counter<>;

  /** "select"/"preselect" method
   *  @see ICaloClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::CaloCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::CaloCluster* cluster ) const override;

  StatusCode initialize() override;
  StatusCode finalize() override;

  /** StNOTard constructor
   *  @param type   tool type
   *  @param name   tool name
   *  @param parent tool parent
   */
  CaloSelectorNOT( const std::string& type, const std::string& name, const IInterface* parent );

private:
  Gaudi::Property<Names> m_selectorsTypeNames{this, "SelectorTools"};
  Selectors              m_selectors;
  mutable IncCounter     m_counter{this, "selected clusters"};
};
// ============================================================================
#endif // CALORECO_CALOSELECTORNOT_H
