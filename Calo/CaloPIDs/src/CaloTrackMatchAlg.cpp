/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloTrackMatchAlg.h"

// ============================================================================
/** @file
 *  Implementation file for class CaloTrackMatchAlg
 *  @date 2006-06-16
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
/// Standard protected constructor
// ============================================================================

template <typename TABLE, typename CALOTYPES>
CaloTrackMatchAlg<TABLE, CALOTYPES>::CaloTrackMatchAlg( const std::string& name, ISvcLocator* pSvc )
    : base_type( name, pSvc, {KeyValue{"Tracks", ""}, KeyValue{"Calos", ""}, KeyValue{"Filter", ""}},
                 KeyValue{"Output", ""} ) {
  // context-dependent default track container
  updateHandleLocation( *this, "Tracks", LHCb::CaloAlgUtils::TrackLocations( context() ).front() );
}

// ============================================================================
/// standard algorithm execution
// ============================================================================
template <typename TABLE, typename CALOTYPES>
TABLE CaloTrackMatchAlg<TABLE, CALOTYPES>::operator()( const LHCb::Tracks& tracks, const CALOTYPES& calos,
                                                       const Filter& filter ) const {

  LHCb::Track::ConstVector good_tracks;
  good_tracks.reserve( 100 );
  std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( good_tracks ), [&]( const auto& track ) {
    if ( !this->use( track ) ) return false;
    const auto& r = filter.relations( track );
    return !r.empty() && r.front().to();
  } );
  const size_t nTracks = good_tracks.size();
  if ( 0 == nTracks )
    if ( msgLevel( MSG::DEBUG ) ) debug() << "No good tracks have been selected" << endmsg;

  TABLE  table( m_tablesize );
  size_t nOverflow = 0;

  // loop over the calo objects
  for ( const auto& calo : calos ) {
    const auto caloPos = position( calo );
    for ( const auto& track : good_tracks ) {
      double     chi2 = 0;
      StatusCode sc   = m_tool->match( caloPos, track, chi2 );
      if ( sc.isFailure() ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Failure from Tool::match, skip" << endmsg;
        m_nMatchFailure += 1;
        continue;
      }
      if ( m_threshold < chi2 ) {
        ++nOverflow;
        continue;
      }
      table.i_push( calo, track, chi2 );
    }
  } // calos
  // MANDATORY: i_sort after i_push
  table.i_sort(); // NB: i_sort

  const auto links = table.i_relations();
  m_nLinks += links.size();
  m_nTracks += nTracks;
  m_nCalos += calos.size();
  m_nOverflow += nOverflow;
  auto b = m_chi2.buffer();
  for ( const auto& l : links ) b += l.weight();

  return table;
}

template class CaloTrackMatchAlg<LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>, LHCb::CaloClusters>;
template class CaloTrackMatchAlg<LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>, LHCb::CaloHypos>;
