/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONIDALG_H
#define MUONIDALG_H 1

// Include files
// from STL
#include <map>
#include <string>
#include <vector>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "MuonDet/MuonBasicGeometry.h"

// From event packages
#include "Event/MuonCoord.h"
#include "Event/MuonPID.h"
#include "Event/Track.h"

#include "MuonDet/DeMuonDetector.h"

#include "IIsMuonCandidateC.h"
#include "INShared.h"
#include "ImuIDTool.h"

#include "TMath.h"

class MuonPID;
class Track;
class MuonCoord;
class DeMuonDetector;

/** @class MuonIDAlg MuonIDAlg.h
 *
 *  This is an Algorithm to create MuonPID objects starting from tracks and
 *  using the hits in the muon system
 *
 *  @author Erica Polycarpo, Miriam Gandelman
 *  @date   20/03/2006
 *
 *
 */
class MuonIDAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  MuonIDAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// Do the identification
  StatusCode doID( LHCb::MuonPID* pMuid );

  /// Calculates the Likelihood given a MuonPID
  StatusCode calcMuonLL( LHCb::MuonPID* muonid );

  /// Calculates the number of tracks that share hits
  StatusCode calcSharedHits( LHCb::MuonPID* muonid, LHCb::MuonPIDs* pMuids );

  /// Calculates the distance from the hit to the extrapolated position
  /// Input: a MuonPID object
  StatusCode calcDist( LHCb::MuonPID* muonid );

  /// Compare the coordinates of two MuonPIDs
  bool compareHits( LHCb::MuonPID* muonid1, LHCb::MuonPID* muonid2 );

  /// check the track is in the p and angle acceptance
  StatusCode preSelection( LHCb::MuonPID* pMuid, bool& passed );

  /// Fill the local vector of coord positions
  StatusCode fillCoordVectors();

  /// Empty the coord vectors
  void clearCoordVectors();

  /// Set the MuonCoords used in this ID
  StatusCode setCoords( LHCb::MuonPID* pMuid );

  /// Extract the momentum and extrapolate the track to each station
  StatusCode trackExtrapolate( const LHCb::Track* pTrack );

  // Find out if st myst is in input array of stations
  template <typename Container>
  bool stInStations( const int myst, const Container& stations ) {
    return std::find( stations.begin(), stations.end(), myst ) != stations.end();
  }

  // Common IsMuon requirements from set of stations with hits in FOI
  bool IsMuon( const std::vector<int>& stations, double p );

  // Common IsMuonLoose requirements from set of stations with hits in FOI
  bool IsMuonLoose( const std::vector<int>& stations, double p );

  // Calculates MuProb based on DeltaSx (slope difference)
  double calcMuProb( LHCb::MuonPID* pMuid );

  /// return the FOI in (x,y) in a station and region for momentum (in MeV/c)
  std::pair<double, double> foiXY( int station, int region, double p ) const;

  /// clear track based local variables
  void resetTrackLocals();

  LHCb::Track* makeMuonTrack( const LHCb::MuonPID& pMuid );

  /// Load ImuIDTool on demand (avoid loading always in initialize())
  inline ImuIDTool* myMuIDTool() {
    if ( !m_myMuIDTool ) m_myMuIDTool = tool<ImuIDTool>( m_myMuIDToolName, "myMuIDTool", this );
    return m_myMuIDTool;
  }

  /// Load ImuIDTool on demand (avoid loading always in initialize())
  inline ImuIDTool* DistMuIDTool() {
    if ( !m_DistMuIDTool ) m_DistMuIDTool = tool<ImuIDTool>( "DistMuIDTool", "DistMuIDTool", this );
    return m_DistMuIDTool;
  }

  /// Load NShared on demand (avoid loading always in initialize())
  inline INShared* NSharedTool() {
    if ( !m_NSharedTool ) m_NSharedTool = tool<INShared>( "NShared", "NSharedTool", this );
    return m_NSharedTool;
  }

  /// Load IsMuonTool on demand (avoid loading always in initialize())
  inline IIsMuonCandidateC* IsMuonTool() {
    if ( !m_IsMuonTool ) m_IsMuonTool = tool<IIsMuonCandidateC>( "IsMuonCandidateC", "IsMuonTool", this );
    return m_IsMuonTool;
  }

  /// Load IsMuonLooseTool on demand (avoid loading always in initialize())
  inline IIsMuonCandidateC* IsMuonLooseTool() {
    if ( !m_IsMuonLooseTool )
      m_IsMuonLooseTool = tool<IIsMuonCandidateC>( "IsMuonCandidateC", "IsMuonLooseTool", this );
    return m_IsMuonLooseTool;
  }

  mutable Gaudi::Accumulators::Counter<> m_goodTracksCount{this, "nGoodTracksForMuonID"};
  mutable Gaudi::Accumulators::Counter<> m_pidsCount{this, "nMuonPIDs"};
  mutable Gaudi::Accumulators::Counter<> m_acceptanceCount{this, "nInAcceptance"};
  mutable Gaudi::Accumulators::Counter<> m_momPreselCount{this, "nMomentumPresel"};
  mutable Gaudi::Accumulators::Counter<> m_isLooseCount{this, "nIsMuonLoose"};
  mutable Gaudi::Accumulators::Counter<> m_isMuonCount{this, "nIsMuon"};
  mutable Gaudi::Accumulators::Counter<> m_isTightCount{this, "nIsMuonTight"};

  // Properties
  // If you add or change a property, change also the options file AND the ConfiguredMuonIDs.py !

  /// TES path of the tracks to analyse
  Gaudi::Property<bool>        m_useTtrack{this, "useTtrack", false};
  Gaudi::Property<std::string> m_TracksPath{this, "TrackLocation", LHCb::TrackLocation::Default,
                                            "Source of track to ID"};

  /// TES path to output the MuonPIDs to
  Gaudi::Property<std::string> m_MuonPIDsPath{this, "MuonIDLocation", LHCb::MuonPIDLocation::Default,
                                              "Destination of MuonPID"};

  /// TES path to output the Track PIDs to
  Gaudi::Property<std::string> m_MuonTracksPath{this, "MuonTrackLocation", LHCb::TrackLocation::Muon,
                                                "Destination of MuonTracks"};

  /// TES path to output the all Track PIDs to
  Gaudi::Property<std::string> m_MuonTracksPathAll{this, "MuonTrackLocationAll",
                                                   LHCb::TrackLocation::Muon + "/AllMuonTracks",
                                                   "Destination of MuonTracks (all tracks)"};

  /// Ignore MuonID info from conditions database.
  Gaudi::Property<bool> m_OverrideDB{this, "OverrideDB", false,
                                     "Ignore MuonID info from conditions database. Use the ones from options file:"};

  /// use dist as muon quality (track extra info)
  bool m_use_dist;

  // Use or not uncrossed logical channels in the non-pad detector areas
  Gaudi::Property<bool> m_use_uncrossed{this, "m_use_uncrossed", true,
                                        "Use or not uncrossed logical channels in the non-pad detector areas"};

  /// Preselection momentum (no attempt to ID below this)
  Gaudi::Property<float> m_PreSelMomentum{this, "PreSelMomentum", 3000.,
                                          "Preselection momentum (no attempt to ID below this)"};

  /// Momentum ranges: different treatement of M4/M5 in each
  Gaudi::Property<std::vector<double>> m_MomentumCuts{
      this, "MomentumCuts", {}, "Different depths of stations considered in different momentum ranges"};

  /// muon and pion distributions
  Gaudi::Property<std::vector<double>> m_distPion{this, "distPion"};
  Gaudi::Property<std::vector<double>> m_distMuon{this, "distMuon"};

  // muon track for KalmanFoI
  LHCb::Track m_mutrack;

  // GL&SF:
  Gaudi::Property<int> m_dllFlag{
      this, "DLL_flag", 1,
      "Flag to discriminate among the different DLLs;"
      "0 -- default"
      "1 -- binned distance with closest hit + integral of Landau fit"
      "3 -- binned tanh(distance) with closest hit(Muon) + integral. Flag 2 used by MuonPIDChecker for monitoring Prob "
      "Mu"
      "4 -- binned tanh(distance) with closest hit(Muon) + integral of Landau fit(NonMuon)."};

  // Use KalmanFoi?
  Gaudi::Property<bool> m_kalman_foi{this, "KalmanFoI", false, "use default FoI or Kalman?"};

  /// GL&SF: Calculates the Distance Likelihood given a MuonPID
  StatusCode calcMuonLL_dist( LHCb::MuonPID* muonid, const double& p );

  /// GL&SF: Get closest hit per station
  StatusCode get_closest( LHCb::MuonPID* pMuid, LHCb::span<double> closest_x, LHCb::span<double> closest_y,
                          LHCb::span<double> closest_region );
  /// GL&SF&XCV: Find the region in a given station of teh extrapolated track
  std::vector<int> findTrackRegions();
  /// GL&SF: Calculate closest distance
  double calc_closestDist( LHCb::MuonPID* pMuid, const double& p, LHCb::span<double> closest_region );
  /// GL&SF: Find parameters for hypothesis test
  StatusCode find_LandauParam( const double& p, const std::vector<int>& trackRegion, std::array<double, 6>& parMu,
                               std::array<double, 3>& parNonMu ) const;
  /// GL&SF: Calculate the compatibility with the Muon hypothesis
  double calc_ProbMu( const double& dist, const std::array<double, 6>& parMu );
  /// GL&SF: Calculate the compatibility with the Non-Muon hypothesis
  double calc_ProbNonMu( const double& dist, const std::array<double, 3>& parNonMu );

  /// GL&SF: Normalizations of the Landaus
  StatusCode calcLandauNorm();
  double     calcNorm( double* par );
  double     calcNorm_nmu( double* par );

  /// Return the momentum bin corresponding to p, given the region
  int GetPbin( double p, int region );

  /// Determine probabilities for DLL_flag=3
  StatusCode calcMuonLL_tanhdist( LHCb::MuonPID* pMuid, const double& p );
  double     calc_ProbMu_tanh( const double& tanhdist0, int pBin, int region );
  double     calc_ProbNonMu_tanh( const double& tanhdist0, int pBin, int region );

  /// Determine probabilities for DLL_flag=4
  StatusCode calcMuonLL_tanhdist_landau( LHCb::MuonPID* pMuid, const double& p );

  double Fdist[5];
  double small_dist[5];
  double closest_region[5];
  double closest_x[5];
  double closest_y[5];

  /// GL&SF: define parameters for the hypothesis test

  Gaudi::Property<std::vector<double>> m_MupBinsR1{this, "MupBinsR1"};
  Gaudi::Property<std::vector<double>> m_MupBinsR2{this, "MupBinsR2"};
  Gaudi::Property<std::vector<double>> m_MupBinsR3{this, "MupBinsR3"};
  Gaudi::Property<std::vector<double>> m_MupBinsR4{this, "MupBinsR4"};

  Gaudi::Property<int> m_nMupBinsR1{this, "nMupBinsR1", 6};
  Gaudi::Property<int> m_nMupBinsR2{this, "nMupBinsR2", 4};
  Gaudi::Property<int> m_nMupBinsR3{this, "nMupBinsR3", 4};
  Gaudi::Property<int> m_nMupBinsR4{this, "nMupBinsR4", 4};

  // Muons - Region2
  Gaudi::Property<std::vector<double>> m_MuLanParR1_1{this, "MuLandauParameterR1_1"};
  Gaudi::Property<std::vector<double>> m_MuLanParR1_2{this, "MuLandauParameterR1_2"};
  Gaudi::Property<std::vector<double>> m_MuLanParR1_3{this, "MuLandauParameterR1_3"};
  Gaudi::Property<std::vector<double>> m_MuLanParR1_4{this, "MuLandauParameterR1_4"};
  Gaudi::Property<std::vector<double>> m_MuLanParR1_5{this, "MuLandauParameterR1_5"};
  Gaudi::Property<std::vector<double>> m_MuLanParR1_6{this, "MuLandauParameterR1_6"};
  Gaudi::Property<std::vector<double>> m_MuLanParR1_7{this, "MuLandauParameterR1_7"};

  // Muons - Region2
  Gaudi::Property<std::vector<double>> m_MuLanParR2_1{this, "MuLandauParameterR2_1"};
  Gaudi::Property<std::vector<double>> m_MuLanParR2_2{this, "MuLandauParameterR2_2"};
  Gaudi::Property<std::vector<double>> m_MuLanParR2_3{this, "MuLandauParameterR2_3"};
  Gaudi::Property<std::vector<double>> m_MuLanParR2_4{this, "MuLandauParameterR2_4"};
  Gaudi::Property<std::vector<double>> m_MuLanParR2_5{this, "MuLandauParameterR2_5"};

  // Muons - Region3
  Gaudi::Property<std::vector<double>> m_MuLanParR3_1{this, "MuLandauParameterR3_1"};
  Gaudi::Property<std::vector<double>> m_MuLanParR3_2{this, "MuLandauParameterR3_2"};
  Gaudi::Property<std::vector<double>> m_MuLanParR3_3{this, "MuLandauParameterR3_3"};
  Gaudi::Property<std::vector<double>> m_MuLanParR3_4{this, "MuLandauParameterR3_4"};
  Gaudi::Property<std::vector<double>> m_MuLanParR3_5{this, "MuLandauParameterR3_5"};

  // Muons - Region4
  Gaudi::Property<std::vector<double>> m_MuLanParR4_1{this, "MuLandauParameterR4_1"};
  Gaudi::Property<std::vector<double>> m_MuLanParR4_2{this, "MuLandauParameterR4_2"};
  Gaudi::Property<std::vector<double>> m_MuLanParR4_3{this, "MuLandauParameterR4_3"};
  Gaudi::Property<std::vector<double>> m_MuLanParR4_4{this, "MuLandauParameterR4_4"};
  Gaudi::Property<std::vector<double>> m_MuLanParR4_5{this, "MuLandauParameterR4_5"};

  // Non-Muons - Region 1-2-3-4:
  Gaudi::Property<std::vector<double>> m_NonMuLanParR1{this, "NonMuLandauParameterR1"};
  Gaudi::Property<std::vector<double>> m_NonMuLanParR2{this, "NonMuLandauParameterR2"};
  Gaudi::Property<std::vector<double>> m_NonMuLanParR3{this, "NonMuLandauParameterR3"};
  Gaudi::Property<std::vector<double>> m_NonMuLanParR4{this, "NonMuLandauParameterR4"};

  Gaudi::Property<float> m_x{this, "step", 0.2, "step for the integral"};
  Gaudi::Property<int>   m_nMax{this, "nMax_bin", 4000, "number of steps"};

  typedef std::vector<std::vector<double>*> vectorOfVectors;

  // tanh scale factors
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsR1{this, "tanhScaleFactorsR1"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsR2{this, "tanhScaleFactorsR2"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsR3{this, "tanhScaleFactorsR3"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsR4{this, "tanhScaleFactorsR4"};
  vectorOfVectors                      m_tanhScaleFactors;

  // tanh(dist2) histograms contents
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_1{this, "tanhCumulHistoMuonR1_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_2{this, "tanhCumulHistoMuonR1_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_3{this, "tanhCumulHistoMuonR1_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_4{this, "tanhCumulHistoMuonR1_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_5{this, "tanhCumulHistoMuonR1_5"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_6{this, "tanhCumulHistoMuonR1_6"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_7{this, "tanhCumulHistoMuonR1_7"};
  vectorOfVectors                      m_tanhCumulHistoMuonR1;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_1{this, "tanhCumulHistoMuonR2_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_2{this, "tanhCumulHistoMuonR2_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_3{this, "tanhCumulHistoMuonR2_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_4{this, "tanhCumulHistoMuonR2_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_5{this, "tanhCumulHistoMuonR2_5"};
  vectorOfVectors                      m_tanhCumulHistoMuonR2;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_1{this, "tanhCumulHistoMuonR3_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_2{this, "tanhCumulHistoMuonR3_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_3{this, "tanhCumulHistoMuonR3_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_4{this, "tanhCumulHistoMuonR3_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_5{this, "tanhCumulHistoMuonR3_5"};
  vectorOfVectors                      m_tanhCumulHistoMuonR3;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_1{this, "tanhCumulHistoMuonR4_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_2{this, "tanhCumulHistoMuonR4_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_3{this, "tanhCumulHistoMuonR4_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_4{this, "tanhCumulHistoMuonR4_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_5{this, "tanhCumulHistoMuonR4_5"};
  vectorOfVectors                      m_tanhCumulHistoMuonR4;

  std::vector<vectorOfVectors*> m_tanhCumulHistoMuon;

  // tanh(dist2) histograms contents
  // # For the moment, non-muons dist per momentum bin come from the same dist per region
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_1{this, "tanhCumulHistoNonMuonR1_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_2{this, "tanhCumulHistoNonMuonR1_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_3{this, "tanhCumulHistoNonMuonR1_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_4{this, "tanhCumulHistoNonMuonR1_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_5{this, "tanhCumulHistoNonMuonR1_5"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_6{this, "tanhCumulHistoNonMuonR1_6"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_7{this, "tanhCumulHistoNonMuonR1_7"};
  vectorOfVectors                      m_tanhCumulHistoNonMuonR1;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_1{this, "tanhCumulHistoNonMuonR2_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_2{this, "tanhCumulHistoNonMuonR2_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_3{this, "tanhCumulHistoNonMuonR2_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_4{this, "tanhCumulHistoNonMuonR2_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_5{this, "tanhCumulHistoNonMuonR2_5"};
  vectorOfVectors                      m_tanhCumulHistoNonMuonR2;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_1{this, "tanhCumulHistoNonMuonR3_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_2{this, "tanhCumulHistoNonMuonR3_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_3{this, "tanhCumulHistoNonMuonR3_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_4{this, "tanhCumulHistoNonMuonR3_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_5{this, "tanhCumulHistoNonMuonR3_5"};
  vectorOfVectors                      m_tanhCumulHistoNonMuonR3;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_1{this, "tanhCumulHistoNonMuonR4_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_2{this, "tanhCumulHistoNonMuonR4_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_3{this, "tanhCumulHistoNonMuonR4_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_4{this, "tanhCumulHistoNonMuonR4_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_5{this, "tanhCumulHistoNonMuonR4_5"};
  vectorOfVectors                      m_tanhCumulHistoNonMuonR4;

  std::vector<vectorOfVectors*> m_tanhCumulHistoNonMuon;

  // want to find quality?
  Gaudi::Property<bool> m_FindQuality{this, "FindQuality", true, "want to find quality?"};

  // Create container with all muonTracks (even if not in acceptance or !IsMuon)
  Gaudi::Property<bool> m_DoAllMuonTracks{
      this, "AllMuonTracks", false, "Create container with all muonTracks (even if not in acceptance or !IsMuon)"};

  // Which MuIDTool should be used
  Gaudi::Property<std::string> m_myMuIDToolName{this, "myMuIDTool", "Chi2MuIDTool"};

  // function that defines the field of interest size
  // formula is p(1) + p(2)*exp(-p(3)*momentum)
  Gaudi::Property<std::vector<double>> m_xfoiParam1{this, "XFOIParameter1"};
  Gaudi::Property<std::vector<double>> m_xfoiParam2{this, "XFOIParameter2"};
  Gaudi::Property<std::vector<double>> m_xfoiParam3{this, "XFOIParameter3"};
  Gaudi::Property<std::vector<double>> m_yfoiParam1{this, "YFOIParameter1"};
  Gaudi::Property<std::vector<double>> m_yfoiParam2{this, "YFOIParameter2"};
  Gaudi::Property<std::vector<double>> m_yfoiParam3{this, "YFOIParameter3"};

  // foifactor for MuonID calibration (in opts file, default = 1.)
  Gaudi::Property<float> m_foifactor{this, "FOIfactor", 1.2, "foifactor for MuonID calibration"};

  // Number of tracks with IsMuon = True (monitoring)
  int m_nmu = 0;
  // Number of tracks with IsMuonLoose = true which failed likelihood construction
  int m_mullfail = 0;
  // Number of stations
  int m_NStation = 0;
  // Number of regions
  int m_NRegion = 0;
  // Names of the station
  std::vector<std::string> m_stationNames;
  // Index of M2. Normally m_iM2=1. But m_iM2=0 in case of no M1. Assuming iM3=iM2+1
  int m_iM2;
  // fill local arrays of pad sizes and region sizes
  DeMuonDetector* m_mudet = nullptr;

  // myMuIDtool
  ImuIDTool* m_myMuIDTool = nullptr;

  // DistMuonIDtool
  ImuIDTool* m_DistMuIDTool = nullptr;

  // NSharedtool
  INShared* m_NSharedTool = nullptr;

  // IsMuonTool
  IIsMuonCandidateC* m_IsMuonTool = nullptr;

  // IsMuonLooseTool
  IIsMuonCandidateC* m_IsMuonLooseTool = nullptr;

  // local array of pad sizes in mm
  // all std::vectors here are indexed: [station * m_NRegion + region]
  std::vector<double> m_padSizeX;
  std::vector<double> m_padSizeY;

  // local array of region sizes
  std::vector<double> m_regionInnerX; // inner edge in abs(x)
  std::vector<double> m_regionOuterX; // outer edge in abs(x)

  std::vector<double> m_regionInnerY; // inner edge in abs(y)
  std::vector<double> m_regionOuterY; // outer edge in abs(y)

  // These are indexed [station]
  // std::vector<double> m_stationZ; // station position
  double m_stationZ[5]; // station position

  // local track parameters: momentum and linear extrapolation to each station
  double              m_dist;
  double              m_dist_out;
  double              m_Momentum;    // in MeV/c
  double              m_MomentumPre; // in MeV/c
  double              m_trackSlopeX;
  std::vector<double> m_trackX; // position of track in x(mm) in each station
  std::vector<double> m_trackY; // position of track in y(mm) in each station

  // test if found a hit in the MuonStations
  std::vector<int> m_occupancyAll;
  std::vector<int> m_occupancyWithCrossing; // Requires x,y crossing for IsMuonTight
  std::map<const LHCb::MuonPID*, std::vector<LHCb::MuonCoord*>> m_muonMap;

  // store X of hits for dx/dz matching with track (only need M2/M3)
  std::vector<double> m_CoordX;
  int                 m_xMatchStation; // first station to calculate slope (M2)

  // OK nasty optimisation here, store x,dx,y,dy of each coord to test against
  // track extrapolation
  class coordExtent_ {
  public:
    coordExtent_( double x, double dx, double y, double dy, LHCb::MuonCoord* pCoord )
        : m_x( x ), m_dx( dx ), m_y( y ), m_dy( dy ), m_pCoord( pCoord ){};
    double           m_x;
    double           m_dx;
    double           m_y;
    double           m_dy;
    LHCb::MuonCoord* m_pCoord = nullptr;
  };

  // vector of positions of coords (innner vector coords,
  // outer is [station* m_NRegion + region ]
  std::vector<std::vector<coordExtent_>> m_coordPos;
};
#endif // MUONID_H
