/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MAKEMUONMEASUREMENTS_H
#define MAKEMUONMEASUREMENTS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "MuonDet/DeMuonDetector.h"
#include "TrackInterfaces/IMeasurementProviderProjector.h"

/** @class MakeMuonMeasurements MakeMuonMeasurements.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-16
 */
class MakeMuonMeasurements : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  IMeasurementProviderProjector* m_measProvider = nullptr;
  DeMuonDetector*                m_mudet        = nullptr;

  Gaudi::Property<bool> m_use_uncrossed{this, "UseUncrossed", true,
                                        "Use or not uncrossed logical channels in the non-pad detector areas"};
};
#endif // MAKEMUONMEASUREMENTS_H
