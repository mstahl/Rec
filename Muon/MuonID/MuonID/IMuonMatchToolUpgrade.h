/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONID_IMUONMATCHTOOLUPGRADE_H
#define MUONID_IMUONMATCHTOOLUPGRADE_H 1
#include <tuple>
#include <vector>

#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"
#include "MuonDAQ/CommonMuonHit.h"

namespace LHCb {
  class MuonTileID;
}

/** @class IMuonMatchToolUpgrade IMuonMatchToolUpgrade.h MuonMatch/IMuonMatchToolUpgrade.h
 *
 *  tool to match tracks to the muon detector, starting from pre-selected matching tables
 */

/// TrackMuMatch contains MuonCommonHit, sigma_match, track extrap X, track extrap Y
typedef std::tuple<const CommonMuonHit*, float, float, float> TrackMuMatch;

struct IMuonMatchToolUpgrade : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonMatchToolUpgrade, 3, 0 );

  enum MuonMatchType { NoMatch = 0, Uncrossed = 1, Good = 2 };

  virtual std::pair<float, int> run( const float, std::vector<TrackMuMatch>* bestMatches,
                                     std::vector<TrackMuMatch>* spareMatches = nullptr ) = 0;
};
#endif // MUONID_IMUONMATCHTOOLUPGRADE_H
