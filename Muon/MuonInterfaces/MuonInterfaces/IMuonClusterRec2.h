/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONID_IMUONCLUSTERREC2_H
#define MUONID_IMUONCLUSTERREC2_H 1

// Include files
// from STL
#include <memory>
#include <vector>
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/STLExtensions.h"
#include "MuonInterfaces/IMuonPadRec.h"

struct ClusterVars {
  std::array<int, 4>   ncl{};
  std::array<float, 4> avg_cs{};
};

/** @class IMuonClusterRec2 IMuonClusterRec2.h MuonInterfaces/IMuonClusterRec2.h
 *
 *  Interface to clustering algorithm
 *  @author Marco Santimaria
 *  @date   2017-06-23
 */
struct IMuonClusterRec2 : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonClusterRec2, 1, 0 );
  virtual ClusterVars clusters( LHCb::span<const MuonLogPad> pads ) const = 0;
};
#endif // MUONID_IMUONCLUSTERREC2_H
