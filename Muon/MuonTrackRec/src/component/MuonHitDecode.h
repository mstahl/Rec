/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LIB_MUONHITDECODE_H
#define LIB_MUONHITDECODE_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonInterfaces/IMuonHitDecode.h" // Interface
struct IMuonRawBuffer;
class DeMuonDetector;

/** @class MuonHitDecode MuonHitDecode.h
 *
 *  Parameters:
 *  - NumberRawLocations: Number of raw locations to look for data (default = 1).
 *  Values larger than one will result in looking for data in Prev# / Next#
 *  - SkipHWNumber: Skip the calculation of the hardware numbers. Can be set 'true'
 *    for reconstructing MuonTT tracks as it is faster.
 *
 *
 */

class MuonHitDecode : public extends<GaudiTool, IMuonHitDecode, IIncidentListener> {

public:
  MuonHitDecode( const std::string& type, const std::string& name, const IInterface* parent );

  // main methods
  StatusCode                     decodeRawData() override;
  const std::vector<MuonLogHit>& hits() override {
    if ( !m_hitsDecoded ) decodeRawData();
    return m_hits;
  }
  const std::vector<MuonLogHit>& fakehits() override { return m_fakehits; }
  // specific for Online Monitoring, not implemented here (just avoid compil. warnings)
  int              banksSize( LHCb::RawBank::BankType, std::vector<int>& ) override { return 0; }
  unsigned int     odeErrorWord( int, int ) override { return 0; }
  int              bankVersion() override { return 0; }
  void             dumpRawBanks() override {}
  void             dumpFrame( int, int ) override {}
  bool             mappingIsOld() override { return false; }
  int              l0id() override { return 0; }
  int              bcn() override { return 0; }
  int              cbcn() override { return 0; }
  void             setMultiBunch( int ) override {}
  void             unsetMultiBunch() override {}
  bool             multiBunch() override { return false; }
  int              mbExtraBXPerside() override { return 0; }
  bool             centralBX() override { return true; }
  bool             firstBX() override { return true; }
  bool             lastBX() override { return true; }
  LHCb::MuonTileID tileFromODE( int, int ) override { return 0; }
  int              odeIndex( int ) override { return 0; }
  int              channelsPerQuadrant( int, int ) override { return 0; }
  int              nPadX( int ) override { return 0; }
  int              nPadY( int ) override { return 0; }
  int              nPadXvy( int, int ) override { return 0; }
  int              nPadYvx( int, int ) override { return 0; }
  float            padSizeX( int, int ) override { return 0.; }
  float            padSizeY( int, int ) override { return 0.; }
  float            padSizeXvy( int, int ) override { return 0.; }
  float            padSizeYvx( int, int ) override { return 0.; }
  LHCb::MuonTileID tileFromLogCh( unsigned int, unsigned int, unsigned int, short int, unsigned int ) override {
    return 0;
  }
  bool completeEvent() override { return true; }

  // from GaudiTool
  StatusCode initialize() override;

  // from IIncidentListener
  void handle( const Incident& incident ) override;

private:
  void                                                   clearHits();
  IMuonRawBuffer*                                        m_recTool      = nullptr;
  DeMuonDetector*                                        m_muonDetector = nullptr;
  std::vector<std::pair<LHCb::MuonTileID, unsigned int>> m_tilesAndTDC;
  std::vector<MuonLogHit>                                m_hits;
  std::vector<MuonLogHit>                                m_fakehits;
  bool                                                   m_hitsDecoded = false;
  Gaudi::Property<int>                                   m_TAENum{this, "NumberRawLocations", 1};
  Gaudi::Property<bool>                                  m_skipHWNumber{this, "SkipHWNumber", false};
};
#endif // LIB_MUONHITDECODE_H
